/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TEXTNODE_H
#define TEXTNODE_H

#include <node.h>

class QTextLayout;
class GlyphNodeInterface;
class QTextBlock;
class QColor;
class QTextDocument;
class QSGContext;

class TextNode : public TransformNode
{
public:
    TextNode(QSGContext *);
    ~TextNode();

    enum UpdateFlags {
        UpdateTransform                             = 0x0001,
        UpdateNodes                                 = 0x0002 | UpdateTransform,
        UpdateAlignment                             = 0x0004 | UpdateNodes,
        UpdateLayout                                = 0x0008 | UpdateAlignment,
        UpdateText                                  = 0x0010 | UpdateLayout,
        UpdateAll                                   = UpdateText
    };

    enum TextStyle {
        NormalTextStyle,
        OutlineTextStyle,
        RaisedTextStyle,
        SunkenTextStyle
    };

    QRectF boundingRect() const;

    void setText(const QString &text);
    QString text() const { return m_text; }

    void setUsePixmapCache(bool on);
    inline bool usePixmapCache() const { return m_usePixmapCache; }

    void setOpacity(qreal opacity);
    inline qreal opacity() const { return m_opacity; }

    void setElideMode(Qt::TextElideMode elideMode);
    inline Qt::TextElideMode elideMode() const { return m_elideMode; }

    void setTextDocument(QTextDocument *textDocument);
    inline QTextDocument *textDocument() const { return m_textDocument; }

    void setColor(const QColor &color);
    inline QColor color() const { return m_color; }

    void setStyleColor(const QColor &styleColor);
    inline QColor styleColor() const { return m_styleColor; }

    void setFont(const QFont &font);
    inline QFont font() const { return m_font; }

    void setTextFormat(Qt::TextFormat textFormat);
    inline Qt::TextFormat textFormat() const { return m_textFormat; }

    void setWrapMode(QTextOption::WrapMode wrapMode);
    inline QTextOption::WrapMode wrapMode() const { return m_wrapMode; }

    void setAlignment(Qt::Alignment alignment);
    inline Qt::Alignment alignment() const { return m_alignment; }

    void setTextStyle(TextStyle textStyle);
    TextStyle textStyle() const { return m_textStyle; }

    void setGeometry(const QRectF &geometry);
    inline QRectF geometry() const { return m_geometry; }

    virtual NodeSubType subType() const { return TextNodeSubType; }

    void setHeightRestriction(qreal heightRestriction);
    qreal heightRestriction() const { return m_heightRestriction; }

    void setWidthRestriction(qreal widthRestriction);
    qreal widthRestriction() const { return m_widthRestriction; }

    QSizeF contentsSize();

    void setLinearFiltering(bool on);
    bool linearFiltering() const { return m_linearFiltering; }

    bool isRichText() const { return m_richText; }

private:
    void deleteContent();

    void updateLayout();
    void updateText();
    void updateAlignment();
    void updateTransform();
    void updateNodes();
    virtual void update(uint updateFlags);

    void detectRichText();
    bool isComplexRichText() const;
    QString elidedText() const;

    QPixmap generatedPixmap() const;
    QPixmap richTextImage(const QColor &color, bool suppressColors) const;
    QPixmap wrappedTextImage(const QColor &color) const;

    void addTextBlock(const QPointF &position, const QTextBlock &block,
                      const QColor &overrideColor);
    void addTextLayout(const QPointF &position, QTextLayout *textLayout, const QColor &color);
    GlyphNodeInterface *addGlyphs(const QPointF &position, const QGlyphs &glyphs, const QColor &color);
    void addTextDecorations(const QPointF &position, const QFont &font, const QColor &color,
                            qreal width);

    QString m_text;
    QFont m_font;

    QColor m_color;
    QColor m_styleColor;
    qreal m_opacity;

    bool m_usePixmapCache : 1;
    bool m_richText : 1;
    bool m_linearFiltering : 1;
    bool m_layoutDirty : 1;

    Qt::TextFormat m_textFormat;
    QTextOption::WrapMode m_wrapMode;
    Qt::TextElideMode m_elideMode;
    Qt::Alignment m_alignment;
    TextStyle m_textStyle;

    QRectF m_geometry;

    QTextDocument *m_textDocument;
    QTextLayout *m_textLayout;

    QSizeF m_contentsSize;
    qreal m_idealContentsWidth;
    qreal m_widthRestriction;
    qreal m_heightRestriction;

    QSGContext *m_context;
};

#endif // TEXTNODE_H
