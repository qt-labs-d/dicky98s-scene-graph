INCLUDEPATH += $$PWD/coreapi $$PWD/convenience $$PWD/3d
!contains(QT_CONFIG, egl):DEFINES += QT_NO_EGL

# Core API
HEADERS += $$PWD/coreapi/geometry.h \
    $$PWD/coreapi/material.h \
    $$PWD/coreapi/node.h \
    $$PWD/coreapi/nodeupdater_p.h \
    $$PWD/coreapi/renderer.h \
    $$PWD/coreapi/qmlrenderer.h \
    $$PWD/coreapi/qsgcontext.h \
    scenegraph/coreapi/qsgtexturemanager.h \
    scenegraph/coreapi/qsgtexturemanager_p.h

SOURCES += $$PWD/coreapi/geometry.cpp \
    $$PWD/coreapi/material.cpp \
    $$PWD/coreapi/node.cpp \
    $$PWD/coreapi/nodeupdater.cpp \
    $$PWD/coreapi/renderer.cpp \
    $$PWD/coreapi/qmlrenderer.cpp \
    $$PWD/coreapi/qsgcontext.h \
    scenegraph/coreapi/qsgtexturemanager.cpp


# Convenience API
HEADERS += $$PWD/convenience/areaallocator.h \
    $$PWD/convenience/textnode.h \
    $$PWD/convenience/flatcolormaterial.h \
    $$PWD/convenience/solidrectnode.h \
    $$PWD/convenience/texturematerial.h \
    $$PWD/convenience/utilities.h \
    $$PWD/convenience/vertexcolormaterial.h \

SOURCES += $$PWD/convenience/areaallocator.cpp \
    $$PWD/convenience/textnode.cpp \
    $$PWD/convenience/flatcolormaterial.cpp \
    $$PWD/convenience/solidrectnode.cpp \
    $$PWD/convenience/texturematerial.cpp \
    $$PWD/convenience/utilities.cpp \
    $$PWD/convenience/vertexcolormaterial.cpp \


# 3D API (duplicates with qt3d)
HEADERS += \
    $$PWD/3d/qglnamespace.h \
    $$PWD/3d/qt3dglobal.h \
    $$PWD/3d/qglattributedescription.h \
    $$PWD/3d/qglattributevalue.h \
    $$PWD/3d/qmatrix4x4stack.h \
    $$PWD/3d/qmatrix4x4stack_p.h \
    $$PWD/3d/qarray.h \
    $$PWD/3d/qcustomdataarray.h \
    $$PWD/3d/qcolor4ub.h \


SOURCES += \
    $$PWD/3d/qarray.cpp \
    $$PWD/3d/qmatrix4x4stack.cpp \
    $$PWD/3d/qglattributedescription.cpp \
    $$PWD/3d/qglattributevalue.cpp \
    $$PWD/3d/qglnamespace.cpp \
    $$PWD/3d/qcustomdataarray.cpp \
    $$PWD/3d/qcolor4ub.cpp \


isEmpty(HARFBUZZ_HEADERS): {
    QT_SOURCE_TREE = $$fromfile($$(QTDIR)/.qmake.cache,QT_SOURCE_TREE)
    unix:warning("Adding $$QT_SOURCE_TREE/src/3rdparty/harfbuzz/src to INCLUDEPATH. Override with HARFBUZZ_HEADERS=dir for different location")
    INCLUDEPATH += $$QT_SOURCE_TREE/src/3rdparty/harfbuzz/src
}
else:INCLUDEPATH += $$HARFBUZZ_HEADERS
