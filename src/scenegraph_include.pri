INCLUDEPATH += \
    $$PWD/scenegraph/coreapi \
    $$PWD/scenegraph/convenience \
    $$PWD/scenegraph/3d \
    $$PWD/scenegraphitem \
    $$PWD/adaptationlayers \
    $$PWD/effects \
    $$PWD/canvas \
    $$PWD/graphicsitems \
    $$PWD

QT += opengl declarative
LIBS += -L$$PWD/../lib -lQtSceneGraph
DEFINES += QT_USE_SCENEGRAPH

DEFINES += QML_RUNTIME_TESTING
