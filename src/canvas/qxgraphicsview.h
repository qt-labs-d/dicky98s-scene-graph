
/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef QXGRAPHICSVIEW_H
#define QXGRAPHICSVIEW_H

#include <QtOpenGL/qgl.h>

#include "qmlscene_global.h"

#include <QtDeclarative/qdeclarativeengine.h>
#include <QtDeclarative/qdeclarativecontext.h>

class QxItem;
class QxGraphicsViewPrivate;
class QSGContext;

class QT_SCENEGRAPH_EXPORT QxGraphicsView : public
#ifdef Q_WS_QPA
        QWidget
#else
        QGLWidget
#endif
{
Q_OBJECT
public:
    QxGraphicsView(QWidget * = 0);
    ~QxGraphicsView();

    QxItem *root();

    QDeclarativeEngine* engine() const;
    QDeclarativeContext* rootContext() const;

    void setSceneGraphContext(QSGContext *context);
    QSGContext *sceneGraphContext() const;

    QUrl source() const;
    void setSource(const QUrl&url);



    enum ResizeMode { SizeViewToRootObject, SizeRootObjectToView };
    ResizeMode resizeMode() const;
    void setResizeMode(ResizeMode);

    QxItem *mouseGrabberItem() const;

    QSize sizeHint() const;

Q_SIGNALS:
    void frameStarted();
    void frameEnded();

protected slots:
    void maybeUpdate();

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void resizeEvent(QResizeEvent *);

    virtual void mousePressEvent(QMouseEvent *);
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void keyPressEvent(QKeyEvent *);
    virtual void keyReleaseEvent(QKeyEvent *);
    virtual void showEvent(QShowEvent *);

private:
    void initializeSceneGraph();

    friend class QxGraphicsViewPrivate;
    Q_DISABLE_COPY(QxGraphicsView)
    QxGraphicsViewPrivate *d;
};

#endif // QXGRAPHICSVIEW_H

