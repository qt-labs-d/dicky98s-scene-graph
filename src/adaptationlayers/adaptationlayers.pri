INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/adaptationlayer.h \
    $$PWD/default/default_texturenode.h \
    $$PWD/default/default_rectanglenode.h \
    $$PWD/default/default_glyphnode.h \
    $$PWD/default/default_glyphnode_p.h \
#    $$PWD/threadedtexturemanager.h
    adaptationlayers/qsgpartialuploadtexturemanager.h \
    adaptationlayers/qsgthreadedtexturemanager.h

SOURCES += \
    $$PWD/adaptationlayer.cpp \
    $$PWD/default/default_texturenode.cpp \
    $$PWD/default/default_rectanglenode.cpp \
    $$PWD/default/default_glyphnode.cpp \
    $$PWD/default/default_glyphnode_p.cpp \
#    $$PWD/threadedtexturemanager.cpp
    adaptationlayers/qsgpartialuploadtexturemanager.cpp \
    adaptationlayers/qsgthreadedtexturemanager.cpp

#macx:{
#    SOURCES += adaptationlayers/mactexturemanager.cpp
#    HEADERS += adaptationlayers/mactexturemanager.h
#}


#contains(QT_CONFIG, qpa) {
#    SOURCES += $$PWD/qsgeglfsthreadedtexturemanager.cpp
#    HEADERS += $$PWD/qsgeglfsthreadedtexturemanager.h
#}
