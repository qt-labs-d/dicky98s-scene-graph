#ifndef QSGTHREADEDTEXTUREMANAGER_H
#define QSGTHREADEDTEXTUREMANAGER_H

#include "qsgtexturemanager.h"

class QSGThreadedTextureManagerThread;
class QSGThreadedTextureManagerPrivate;
class QSGThreadedTexture;

class QSGThreadedTextureManager : public QSGTextureManager
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QSGThreadedTextureManager);

public:
    QSGThreadedTextureManager();

    QSGTextureRef upload(const QImage &image);
    QSGTextureRef requestUpload(const QImage &image, const QObject *listener, const char *slot);

protected:
    virtual void createThreadContext();
    virtual void makeThreadContextCurrent();
    virtual void uploadInThread(const QImage &image, QSGTexture *texture);

private:
    friend class QSGThreadedTextureManagerThread;
    friend class QSGThreadedTexture;
};

#endif // QSGTHREADEDTEXTUREMANAGER_H
