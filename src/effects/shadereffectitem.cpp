/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "shadereffectitem.h"

#include "utilities.h"
#include "material.h"
#include "qxitem_p.h"

#include "qsgcontext.h"

#include <QtCore/qsignalmapper.h>
#include <QtOpenGL/qglframebufferobject.h>
#include <QtGui/qimagereader.h>

static const char qt_default_vertex_code[] =
    "uniform highp mat4 qt_ModelViewProjectionMatrix;               \n"
    "attribute highp vec4 qt_Vertex;                                \n"
    "attribute highp vec2 qt_MultiTexCoord0;                        \n"
    "varying highp vec2 qt_TexCoord0;                               \n"
    "void main() {                                                  \n"
    "    qt_TexCoord0 = qt_MultiTexCoord0;                          \n"
    "    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;    \n"
    "}";

static const char qt_default_fragment_code[] =
    "varying highp vec2 qt_TexCoord0;                       \n"
    "uniform lowp sampler2D source;                         \n"
    "void main() {                                          \n"
    "    gl_FragColor = texture2D(source, qt_TexCoord0);    \n"
    "}";

class CustomShaderMaterialData : public AbstractEffectProgram
{
public:
    CustomShaderMaterialData(ShaderEffectItem *q) : q(q) { Q_ASSERT(q); }
    virtual void activate();
    virtual void deactivate();
    virtual void updateRendererState(Renderer *renderer, Renderer::Updates updates);
    virtual void updateEffectState(Renderer *renderer, AbstractEffect *newEffect, AbstractEffect *oldEffect);
    virtual const QGL::VertexAttribute *requiredFields() const;

    ShaderEffectItem *q;
};

void CustomShaderMaterialData::activate()
{
    if (q->m_program_dirty)
        q->updateShaderProgram();

    q->m_program.bind();
    for (int i = 0; i < q->m_attributeNames.size(); ++i)
        q->m_program.enableAttributeArray(q->m_attributes.at(i));
}

void CustomShaderMaterialData::deactivate()
{
    for (int i = 0; i < q->m_attributeNames.size(); ++i)
        q->m_program.disableAttributeArray(q->m_attributes.at(i));
}

void CustomShaderMaterialData::updateRendererState(Renderer *renderer, Renderer::Updates updates)
{
    if ((updates & Renderer::UpdateMatrices) && q->m_respects_matrix)
        q->m_program.setUniformValue("qt_ModelViewProjectionMatrix", renderer->combinedMatrix());
}

void CustomShaderMaterialData::updateEffectState(Renderer *r, AbstractEffect *newEffect, AbstractEffect *oldEffect)
{
    Q_ASSERT(oldEffect == 0);
    Q_ASSERT(newEffect == static_cast<AbstractEffect *>(q));
    Q_UNUSED(oldEffect);
    Q_UNUSED(newEffect);

    for (int i = q->m_sources.size() - 1; i >= 0; --i) {
        const ShaderEffectItem::SourceData &source = q->m_sources.at(i);
        if (!source.source)
            continue;

        r->glActiveTexture(GL_TEXTURE0 + i);
        source.source->bind();
    }

    if (q->m_respects_opacity)
        q->m_program.setUniformValue("qt_Opacity", (float) q->renderOpacity());

    QSet<QByteArray>::const_iterator it;
    for (it = q->m_uniformNames.begin(); it != q->m_uniformNames.end(); ++it) {
        const QByteArray &name = *it;
        QVariant v = q->property(name.constData());

        switch (v.type()) {
        case QVariant::Color:
            q->m_program.setUniformValue(name.constData(), qvariant_cast<QColor>(v));
            break;
        case QVariant::Double:
            q->m_program.setUniformValue(name.constData(), (float) qvariant_cast<double>(v));
            break;
        case QVariant::Transform:
            q->m_program.setUniformValue(name.constData(), qvariant_cast<QTransform>(v));
            break;
        case QVariant::Int:
            q->m_program.setUniformValue(name.constData(), v.toInt());
            break;
        case QVariant::Size:
        case QVariant::SizeF:
            q->m_program.setUniformValue(name.constData(), v.toSizeF());
            break;
        case QVariant::Point:
        case QVariant::PointF:
            q->m_program.setUniformValue(name.constData(), v.toPointF());
            break;
        case QVariant::Rect:
        case QVariant::RectF:
            {
                QRectF r = v.toRectF();
                q->m_program.setUniformValue(name.constData(), r.x(), r.y(), r.width(), r.height());
            }
            break;
        case QVariant::Vector3D:
            q->m_program.setUniformValue(name.constData(), qvariant_cast<QVector3D>(v));
            break;
        default:
            break;
        }
    }
}

const QGL::VertexAttribute *CustomShaderMaterialData::requiredFields() const
{
    if (q->m_program_dirty)
        q->updateShaderProgram();
    return q->m_attributes.constData();
}


ShaderEffectSource::ShaderEffectSource(QObject *parent)
    : QObject(parent)
    , m_sourceItem(0)
    , m_mipmap(None)
    , m_filtering(Nearest)
    , m_horizontalWrap(ClampToEdge)
    , m_verticalWrap(ClampToEdge)
    , m_margins(0, 0)
    , m_size(0, 0)
    , m_fbo(0)
    , m_multisampledFbo(0)
    , m_renderer(0)
    , m_refs(0)
    , m_dirtyTexture(true)
    , m_dirtySceneGraph(true)
    , m_multisamplingSupported(false)
    , m_checkedForMultisamplingSupport(false)
    , m_live(true)
    , m_hideOriginal(true)
{
    m_context = QSGContext::current;
}

ShaderEffectSource::~ShaderEffectSource()
{
    if (m_refs && m_sourceItem)
        QxItemPrivate::get(m_sourceItem)->derefFromEffectItem();
    delete m_fbo;
    delete m_multisampledFbo;
    delete m_renderer;
}

void ShaderEffectSource::setSourceItem(QxItem *item)
{
    if (item == m_sourceItem)
        return;

    if (m_sourceItem) {
        disconnect(m_sourceItem, SIGNAL(widthChanged()), this, SLOT(markSourceSizeDirty()));
        disconnect(m_sourceItem, SIGNAL(heightChanged()), this, SLOT(markSourceSizeDirty()));
        if (m_refs && m_hideOriginal) {
            Q_ASSERT(m_renderer);
            QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
            d->derefFromEffectItem();
            m_renderer->setRootNode(0);
        }
    }

    m_sourceItem = item;

    if (m_sourceItem && !m_renderer) {
        m_renderer = m_context->createRenderer();
        connect(m_renderer, SIGNAL(sceneGraphChanged()), this, SLOT(markSceneGraphDirty()));
    }

    if (m_sourceItem) {
        if (m_refs && m_hideOriginal) {
            Q_ASSERT(m_renderer);
            QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
            d->refFromEffectItem();
            m_renderer->setRootNode(d->rootNode);
        }
        connect(m_sourceItem, SIGNAL(widthChanged()), this, SLOT(markSourceSizeDirty()));
        connect(m_sourceItem, SIGNAL(heightChanged()), this, SLOT(markSourceSizeDirty()));
    }

    updateSizeAndTexture();
    emit sourceItemChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setSourceImage(const QUrl &url)
{
    if (url == m_sourceImage)
        return;
    m_sourceImage = url;
    updateSizeAndTexture();
    emit sourceImageChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setMipmap(FilterMode mode)
{
    if (mode == m_mipmap)
        return;
    m_mipmap = mode;
    if (m_mipmap != None) {
        // Mipmap enabled, need to reallocate the textures.
        if (m_fbo && !m_fbo->format().mipmap()) {
            Q_ASSERT(m_sourceItem);
            delete m_fbo;
            m_fbo = 0;
            m_dirtyTexture = true;
        } else if (!m_texture.isNull()) {
            Q_ASSERT(!m_sourceImage.isEmpty());
            if (!m_texture->hasMipmaps())
                updateSizeAndTexture();
        }
    }
    emit mipmapChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setFiltering(FilterMode mode)
{
    if (mode == m_filtering)
        return;
    m_filtering = mode;
    emit filteringChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setHorizontalWrap(WrapMode mode)
{
    if (mode == m_horizontalWrap)
        return;
    m_horizontalWrap = mode;
    emit horizontalWrapChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setVerticalWrap(WrapMode mode)
{
    if (mode == m_verticalWrap)
        return;
    m_verticalWrap = mode;
    emit verticalWrapChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setMargins(const QSize &size)
{
    if (size == m_margins)
        return;
    m_margins = size;
    updateSizeAndTexture();
    emit marginsChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setTextureSize(const QSize &size)
{
    if (size == m_textureSize)
        return;
    m_textureSize = size;
    updateSizeAndTexture();
    emit textureSizeChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setLive(bool s)
{
    if (s == m_live)
        return;
    m_live = s;

    emit liveChanged();
    emit repaintRequired();
}

void ShaderEffectSource::setHideOriginal(bool hide)
{
    if (hide == m_hideOriginal)
        return;

    if (m_refs && m_sourceItem && m_hideOriginal) {
        Q_ASSERT(m_renderer);
        m_renderer->setRootNode(0);
        QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
        d->derefFromEffectItem();
    }

    m_hideOriginal = hide;

    if (m_refs && m_sourceItem && m_hideOriginal) {
        Q_ASSERT(m_renderer);
        QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
        d->refFromEffectItem();
        m_renderer->setRootNode(d->rootNode);
    }

    emit hideOriginalChanged();
    emit repaintRequired();
}

void ShaderEffectSource::bind() const
{
    bool linear = m_filtering == Linear;

    GLint filtering = GL_NEAREST;
    switch (m_mipmap) {
    case Nearest:
        filtering = linear ? GL_LINEAR_MIPMAP_NEAREST : GL_NEAREST_MIPMAP_NEAREST;
        break;
    case Linear:
        filtering = linear ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR;
        break;
    default:
        filtering = linear ? GL_LINEAR : GL_NEAREST;
        break;
    }

    GLuint hwrap = m_horizontalWrap == Repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE;
    GLuint vwrap = m_verticalWrap == Repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE;

#if !defined(QT_OPENGL_ES_2)
    glEnable(GL_TEXTURE_2D);
#endif
    if (m_fbo) {
        glBindTexture(GL_TEXTURE_2D, m_fbo->texture());
    } else if (!m_texture.isNull()) {
        glBindTexture(GL_TEXTURE_2D, m_texture->textureId());
    } else {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linear ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, hwrap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, vwrap);
}

void ShaderEffectSource::refFromEffectItem()
{
    if (m_refs++ == 0) {
        if (m_sourceItem && m_hideOriginal) {
            Q_ASSERT(m_renderer);
            QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
            d->refFromEffectItem();
            m_renderer->setRootNode(d->rootNode);
        }
        emit activeChanged();
    }
}

void ShaderEffectSource::derefFromEffectItem()
{
    if (--m_refs == 0) {
        if (m_sourceItem && m_hideOriginal) {
            Q_ASSERT(m_renderer);
            m_renderer->setRootNode(0);
            QxItemPrivate *d = QxItemPrivate::get(m_sourceItem);
            d->derefFromEffectItem();
        }
        emit activeChanged();
    }
    Q_ASSERT(m_refs >= 0);
}

void ShaderEffectSource::update()
{
    Q_ASSERT(m_refs);
    QxItemPrivate *src = m_sourceItem ? QxItemPrivate::get(m_sourceItem) : 0;
    Node::DirtyFlags dirtyFlags = src ? src->transformNode.dirtyFlags() : Node::DirtyFlags(0);
    if (!m_dirtyTexture && (!(m_dirtySceneGraph || dirtyFlags) || !m_live))
        return;
    if (m_sourceItem) {
        Q_ASSERT(m_hideOriginal == (m_renderer->rootNode() != 0));
        if (!m_hideOriginal) {
            src->refFromEffectItem();
            m_renderer->setRootNode(src->rootNode);
        }
        Q_ASSERT(src->rootNode && src->rootNode == m_renderer->rootNode());
        Q_ASSERT(m_renderer);

        const QGLContext *ctx = m_context->glContext();

        if (!m_checkedForMultisamplingSupport) {
            QList<QByteArray> extensions = QByteArray((const char *)glGetString(GL_EXTENSIONS)).split(' ');
            m_multisamplingSupported = extensions.contains("GL_EXT_framebuffer_multisample")
                                       && extensions.contains("GL_EXT_framebuffer_blit");
            m_checkedForMultisamplingSupport = true;
        }

        if (!m_fbo) {
            if (ctx->format().sampleBuffers() && m_multisamplingSupported) {
                // If mipmapping was just enabled, m_fbo might be 0 while m_multisampledFbo != 0.
                if (!m_multisampledFbo) {
                    QGLFramebufferObjectFormat format;
                    format.setAttachment(QGLFramebufferObject::CombinedDepthStencil);
                    format.setSamples(ctx->format().samples());
                    m_multisampledFbo = new QGLFramebufferObject(m_size, format);
                }
                {
                    QGLFramebufferObjectFormat format;
                    format.setAttachment(QGLFramebufferObject::NoAttachment);
                    format.setMipmap(m_mipmap != None);
                    m_fbo = new QGLFramebufferObject(m_size, format);
                }
            } else {
                QGLFramebufferObjectFormat format;
                format.setAttachment(QGLFramebufferObject::CombinedDepthStencil);
                format.setMipmap(m_mipmap != None);
                m_fbo = new QGLFramebufferObject(m_size, format);
            }
        }

        Q_ASSERT(m_size == m_fbo->size());
        Q_ASSERT(m_multisampledFbo == 0 || m_size == m_multisampledFbo->size());

        QRectF r(0, 0, m_sourceItem->width(), m_sourceItem->height());
        r.adjust(-m_margins.width(), -m_margins.height(), m_margins.width(), m_margins.height());
        m_renderer->setDeviceRect(m_size);
        m_renderer->setProjectMatrixToRect(r);
        m_renderer->setClearColor(Qt::transparent);

        if (m_multisampledFbo) {
            m_renderer->renderScene(BindableFbo(const_cast<QGLContext *>(m_context->glContext()), m_multisampledFbo));
            QRect r(0, 0, m_size.width(), m_size.height());
            QGLFramebufferObject::blitFramebuffer(m_fbo, r, m_multisampledFbo, r);
        } else {
            m_renderer->renderScene(BindableFbo(const_cast<QGLContext *>(m_context->glContext()), m_fbo));
        }
        if (m_mipmap != None) {
            QGLFramebufferObject::bindDefault();
            glBindTexture(GL_TEXTURE_2D, m_fbo->texture());
            m_context->renderer()->glGenerateMipmap(GL_TEXTURE_2D);
        }

        if (!m_hideOriginal) {
            m_renderer->setRootNode(0);
            src->derefFromEffectItem();
            src->transformNode.markDirty(dirtyFlags);
        }

        m_dirtySceneGraph = false;
    }
    m_dirtyTexture = false;
}

void ShaderEffectSource::grab()
{
    m_dirtyTexture = true;
    emit repaintRequired();
}

void ShaderEffectSource::markSceneGraphDirty()
{
    m_dirtySceneGraph = true;
    emit repaintRequired();
}

void ShaderEffectSource::markSourceSizeDirty()
{
    Q_ASSERT(m_sourceItem);
    if (m_textureSize.isEmpty())
        updateSizeAndTexture();
    if (m_refs)
        emit repaintRequired();
}

void ShaderEffectSource::updateSizeAndTexture()
{
    if (m_sourceItem) {
        QSize size = m_textureSize;
        if (size.isEmpty())
            size = m_sourceItem->size().toSize() + 2 * m_margins;
        if (size.width() < 1)
            size.setWidth(1);
        if (size.height() < 1)
            size.setHeight(1);
        if (m_fbo && m_fbo->size() != size) {
            delete m_fbo;
            delete m_multisampledFbo;
            m_fbo = m_multisampledFbo = 0;
        }
        if (m_size.width() != size.width()) {
            m_size.setWidth(size.width());
            emit widthChanged();
        }
        if (m_size.height() != size.height()) {
            m_size.setHeight(size.height());
            emit heightChanged();
        }
        m_dirtyTexture = true;
    } else {
        if (m_fbo) {
            delete m_fbo;
            delete m_multisampledFbo;
            m_fbo = m_multisampledFbo = 0;
        }
        if (!m_sourceImage.isEmpty()) {
            // TODO: Implement async loading and loading over network.
            QImageReader reader(m_sourceImage.toLocalFile());
            if (!m_textureSize.isEmpty())
                reader.setScaledSize(m_textureSize);
            QImage image = reader.read();
            if (image.isNull())
                qWarning() << reader.errorString();
            if (m_size.width() != image.width()) {
                m_size.setWidth(image.width());
                emit widthChanged();
            }
            if (m_size.height() != image.height()) {
                m_size.setHeight(image.height());
                emit heightChanged();
            }
            QSGTextureManager *tm = m_context->textureManager();
            m_texture = tm->upload(image.mirrored());

            if (m_mipmap) {
                glBindTexture(GL_TEXTURE_2D, m_texture->textureId());
                m_context->renderer()->glGenerateMipmap(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, 0);
            }

        } else {
            if (m_size.width() != 0) {
                m_size.setWidth(0);
                emit widthChanged();
            }
            if (m_size.height() != 0) {
                m_size.setHeight(0);
                emit heightChanged();
            }
        }
    }
}

ShaderEffectNode::ShaderEffectNode()
    : m_meshResolution(1, 1)
{
    setFlag(UsePreprocess);
    setGeometry(Utilities::createTexturedRectGeometry(QRectF(0, 0, 1, 1), QSize(1, 1), QRectF(0, 1, 1, -1)));
}

void ShaderEffectNode::setRect(const QRectF &rect)
{
    setUpdateFlag(UpdateGeometry);
    setBoundingRect(rect);
}

QRectF ShaderEffectNode::rect() const
{
    return boundingRect();
}

void ShaderEffectNode::setResolution(const QSize &res)
{
    setUpdateFlag(UpdateGeometry);
    m_meshResolution = res;
}

QSize ShaderEffectNode::resolution() const
{
    return m_meshResolution;
}

void ShaderEffectNode::update(uint updateFlags)
{
    if (updateFlags & UpdateGeometry)
        updateGeometry();
}

void ShaderEffectNode::updateGeometry()
{
    int vmesh = m_meshResolution.height();
    int hmesh = m_meshResolution.width();

    Geometry *g = geometry();
    g->setVertexCount((vmesh + 1) * (hmesh + 1));
    g->setIndexCount(vmesh * 2 * (hmesh + 2));

    struct V { float x, y, tx, ty; };

    V *vdata = (V *) g->vertexData();

    QRectF dstRect = boundingRect();
    QRectF srcRect(0, 1, 1, -1);
    for (int iy = 0; iy <= vmesh; ++iy) {
        float fy = iy / float(vmesh);
        float y = float(dstRect.top()) + fy * float(dstRect.height());
        float ty = float(srcRect.top()) + fy * float(srcRect.height());
        for (int ix = 0; ix <= hmesh; ++ix) {
            float fx = ix / float(hmesh);
            vdata->x = float(dstRect.left()) + fx * float(dstRect.width());
            vdata->y = y;
            vdata->tx = float(srcRect.left()) + fx * float(srcRect.width());
            vdata->ty = ty;
            ++vdata;
        }
    }

    quint16 *indices = (quint16 *)g->ushortIndexData();
    int i = 0;
    for (int iy = 0; iy < vmesh; ++iy) {
        *(indices++) = i;
        for (int ix = 0; ix <= hmesh; ++ix) {
            *(indices++) = i++;
            *(indices++) = i + hmesh;
        }
        *(indices++) = i + hmesh;
    }
    Q_ASSERT(indices == g->ushortIndexData() + g->indexCount());

    markDirty(Node::DirtyGeometry);
}

void ShaderEffectNode::preprocess()
{
    ShaderEffectItem *item = static_cast<ShaderEffectItem *>(material());
    Q_ASSERT(item);
    item->preprocess();
}


ShaderEffectItem::ShaderEffectItem(QxItem *parent)
    : QxItem(parent)
    , m_mesh_resolution(1, 1)
    , m_program_dirty(true)
    , m_active(true)
    , m_respects_matrix(false)
    , m_respects_opacity(false)
{
    setFlags(Blending);
    m_node.setMaterial(this);
    setPaintNode(&m_node);
}

ShaderEffectItem::~ShaderEffectItem()
{
    reset();
}

void ShaderEffectItem::componentComplete()
{
    m_node.setRect(QRectF(QPointF(), size()));
    updateProperties();
    QxItem::componentComplete();
}

AbstractEffectType *ShaderEffectItem::type() const
{
    return const_cast<AbstractEffectType *>(&m_type);
}

AbstractEffectProgram *ShaderEffectItem::createProgram() const
{
    return new CustomShaderMaterialData(const_cast<ShaderEffectItem *>(this));
}

void ShaderEffectItem::setFragmentShader(const QString &code)
{
    if (m_fragment_code.constData() == code.constData())
        return;
    m_fragment_code = code;
    if (isComponentComplete()) {
        reset();
        updateProperties();
    }
    emit fragmentShaderChanged();
}

void ShaderEffectItem::setVertexShader(const QString &code)
{
    if (m_vertex_code.constData() == code.constData())
        return;
    m_vertex_code = code;
    if (isComponentComplete()) {
        reset();
        updateProperties();
    }
    emit vertexShaderChanged();
}

void ShaderEffectItem::setBlending(bool enable)
{
    if (blending() == enable)
        return;
    setFlags(enable ? Blending : Flag(0));
    m_node.markDirty(Node::DirtyMaterial);
    emit blendingChanged();
}

void ShaderEffectItem::setActive(bool enable)
{
    if (m_active == enable)
        return;

    if (m_active) {
        for (int i = 0; i < m_sources.size(); ++i) {
            ShaderEffectSource *source = m_sources.at(i).source;
            if (!source)
                continue;
            disconnect(source, SIGNAL(repaintRequired()), this, SLOT(markDirty()));
            source->derefFromEffectItem();
        }
        setPaintNode(0);
    }

    m_active = enable;

    if (m_active) {
        setPaintNode(&m_node);
        for (int i = 0; i < m_sources.size(); ++i) {
            ShaderEffectSource *source = m_sources.at(i).source;
            if (!source)
                continue;
            source->refFromEffectItem();
            connect(source, SIGNAL(repaintRequired()), this, SLOT(markDirty()));
        }
    }

    emit activeChanged();
}

void ShaderEffectItem::setMeshResolution(const QSize &size)
{
    if (size == m_mesh_resolution)
        return;
    m_node.setResolution(size);
}

void ShaderEffectItem::preprocess()
{
    for (int i = 0; i < m_sources.size(); ++i) {
        ShaderEffectSource *source = m_sources.at(i).source;
        if (source)
            source->update();
    }
}

void ShaderEffectItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (newGeometry.size() != oldGeometry.size())
        m_node.setRect(QRectF(QPointF(), newGeometry.size()));
    QxItem::geometryChanged(newGeometry, oldGeometry);
}

void ShaderEffectItem::changeSource(int index)
{
    Q_ASSERT(index >= 0 && index < m_sources.size());
    QVariant v = property(m_sources.at(index).name.constData());
    setSource(v, index);
}

void ShaderEffectItem::markDirty()
{
    m_node.markDirty(Node::DirtyMaterial);
}

void ShaderEffectItem::setSource(QVariant var, int index)
{
    Q_ASSERT(index >= 0 && index < m_sources.size());

    SourceData &source = m_sources[index];

    if (m_active && source.source) {
        disconnect(source.source, SIGNAL(repaintRequired()), this, SLOT(markDirty()));
        source.source->derefFromEffectItem();
    }

    enum SourceType { Url, Item, Source, Other };
    SourceType sourceType = Other;
    QObject *obj = 0;

    if (!var.isValid()) {
        sourceType = Source; // Causes source to be set to null.
    } else if (var.type() == QVariant::Url || var.type() == QVariant::String) {
        sourceType = Url;
    } else if (var.type() == QMetaType::QObjectStar) {
        obj = qVariantValue<QObject *>(var);
        if (qobject_cast<QxItem *>(obj))
            sourceType = Item;
        else if (!obj || qobject_cast<ShaderEffectSource *>(obj)) // Interpret null as ShaderEffectSource.
            sourceType = Source;
    }

    switch (sourceType) {
    case Url:
        {
            QUrl url = var.type() == QVariant::Url ? var.toUrl() : QUrl(var.toString());
            if (source.ownedByEffect && !url.isEmpty() && source.source->sourceImage() == url)
                break;
            if (source.ownedByEffect)
                delete source.source;
            source.source = new ShaderEffectSource;
            source.source->setSceneGraphContext(QSGContext::current);
            source.ownedByEffect = true;
            source.source->setSourceImage(url);
        }
        break;
    case Item:
        if (source.ownedByEffect && source.source->sourceItem() == obj)
            break;
        if (source.ownedByEffect)
            delete source.source;
        source.source = new ShaderEffectSource;
        source.source->setSceneGraphContext(QSGContext::current);
        source.ownedByEffect = true;
        source.source->setSourceItem(static_cast<QxItem *>(obj));
        break;
    case Source:
        if (obj == source.source)
            break;
        if (source.ownedByEffect)
            delete source.source;
        source.source = static_cast<ShaderEffectSource *>(obj);
        source.ownedByEffect = false;
        break;
    default:
        qWarning("Could not assign source of type '%s' to property '%s'.", var.typeName(), source.name.constData());
        break;
    }

    if (m_active && source.source) {
        source.source->refFromEffectItem();
        connect(source.source, SIGNAL(repaintRequired()), this, SLOT(markDirty()));
    }
}

void ShaderEffectItem::disconnectPropertySignals()
{
    disconnect(this, 0, this, SLOT(markDirty()));
    for (int i = 0; i < m_sources.size(); ++i) {
        SourceData &source = m_sources[i];
        disconnect(this, 0, source.mapper, 0);
        disconnect(source.mapper, 0, this, 0);
    }
}

void ShaderEffectItem::connectPropertySignals()
{
    QSet<QByteArray>::const_iterator it;
    for (it = m_uniformNames.begin(); it != m_uniformNames.end(); ++it) {
        int pi = metaObject()->indexOfProperty(it->constData());
        if (pi >= 0) {
            QMetaProperty mp = metaObject()->property(pi);
            if (!mp.hasNotifySignal())
                qWarning("ShaderEffectItem: property '%s' does not have notification method!", it->constData());
            QByteArray signalName("2");
            signalName.append(mp.notifySignal().signature());
            connect(this, signalName, this, SLOT(markDirty()));
        } else {
            qWarning("ShaderEffectItem: '%s' does not have a matching property!", it->constData());
        }
    }
    for (int i = 0; i < m_sources.size(); ++i) {
        SourceData &source = m_sources[i];
        int pi = metaObject()->indexOfProperty(source.name.constData());
        if (pi >= 0) {
            QMetaProperty mp = metaObject()->property(pi);
            QByteArray signalName("2");
            signalName.append(mp.notifySignal().signature());
            connect(this, signalName, source.mapper, SLOT(map()));
            source.mapper->setMapping(this, i);
            connect(source.mapper, SIGNAL(mapped(int)), this, SLOT(changeSource(int)));
        } else {
            qWarning("ShaderEffectItem: '%s' does not have a matching source!", source.name.constData());
        }
    }
}

void ShaderEffectItem::reset()
{
    disconnectPropertySignals();

    m_program.removeAllShaders();
    m_attributeNames.clear();
    m_attributes.clear();
    m_uniformNames.clear();
    for (int i = 0; i < m_sources.size(); ++i) {
        const SourceData &source = m_sources.at(i);
        if (m_active && source.source)
            source.source->derefFromEffectItem();
        delete source.mapper;
        if (source.ownedByEffect)
            delete source.source;
    }

    m_sources.clear();

    m_node.markDirty(Node::DirtyMaterial);
    m_program_dirty = true;
}

void ShaderEffectItem::updateProperties()
{
    QString vertexCode = m_vertex_code;
    QString fragmentCode = m_fragment_code;
    if (vertexCode.isEmpty())
        vertexCode = QString::fromLatin1(qt_default_vertex_code);
    if (fragmentCode.isEmpty())
        fragmentCode = QString::fromLatin1(qt_default_fragment_code);

    lookThroughShaderCode(vertexCode);
    lookThroughShaderCode(fragmentCode);

    for (int i = 0; i < m_sources.size(); ++i) {
        QVariant v = property(m_sources.at(i).name);
        setSource(v, i); // Property exists.
    }

    // Append an 'end of array' marker so that m_attributes.constData() can be returned in requiredFields().
    m_attributes.append(QGL::VertexAttribute(-1));
    connectPropertySignals();
}

void ShaderEffectItem::updateShaderProgram()
{
    Q_ASSERT(m_program_dirty);

    QString vertexCode = m_vertex_code;
    QString fragmentCode = m_fragment_code;
    if (vertexCode.isEmpty())
        vertexCode = QString::fromLatin1(qt_default_vertex_code);
    if (fragmentCode.isEmpty())
        fragmentCode = QString::fromLatin1(qt_default_fragment_code);

    m_program.addShaderFromSourceCode(QGLShader::Vertex, vertexCode);
    m_program.addShaderFromSourceCode(QGLShader::Fragment, fragmentCode);

    for (int i = 0; i < m_attributeNames.size(); ++i)
        m_program.bindAttributeLocation(m_attributeNames.at(i), m_attributes.at(i));

    if (!m_program.link()) {
        qWarning("ShaderEffectItem: Shader compilation failed:");
        qWarning() << m_program.log();
    }

    if (!m_attributes.contains(QGL::Position))
        qWarning("ShaderEffectItem: Missing reference to \'qt_Vertex\'.");
    if (!m_attributes.contains(QGL::TextureCoord0))
        qWarning("ShaderEffectItem: Missing reference to \'qt_MultiTexCoord0\'.");
    if (!m_respects_matrix)
        qWarning("ShaderEffectItem: Missing reference to \'qt_ModelViewProjectionMatrix\'.");

    if (m_program.isLinked()) {
        m_program.bind();
        for (int i = 0; i < m_sources.size(); ++i)
            m_program.setUniformValue(m_sources.at(i).name.constData(), i);
    }

    m_program_dirty = false;
}

void ShaderEffectItem::lookThroughShaderCode(const QString &code)
{
    // Regexp for matching attributes and uniforms.
    // In human readable form: attribute|uniform [lowp|mediump|highp] <type> <name>
    static QRegExp re(QLatin1String("\\b(attribute|uniform)\\b\\s*\\b(?:lowp|mediump|highp)?\\b\\s*\\b(\\w+)\\b\\s*\\b(\\w+)"));
    Q_ASSERT(re.isValid());

    int pos = -1;
    while ((pos = re.indexIn(code, pos + 1)) != -1) {
        QString decl = re.cap(1); // uniform or attribute
        QString type = re.cap(2); // type
        QString name = re.cap(3); // variable name

        if (decl == QLatin1String("attribute")) {
            if (name == QLatin1String("qt_Vertex")) {
                m_attributeNames.append(name.toLatin1());
                m_attributes.append(QGL::Position);
            } else if (name == QLatin1String("qt_MultiTexCoord0")) {
                m_attributeNames.append(name.toLatin1());
                m_attributes.append(QGL::TextureCoord0);
            } else {
                // TODO: Support user defined attributes.
                qWarning("ShaderEffectItem: Attribute \'%s\' not recognized.", qPrintable(name));
            }
        } else {
            Q_ASSERT(decl == QLatin1String("uniform"));

            if (name == QLatin1String("qt_ModelViewProjectionMatrix")) {
                m_respects_matrix = true;
            } else if (name == QLatin1String("qt_Opacity")) {
                m_respects_opacity = true;
            } else {
                m_uniformNames.insert(name.toLatin1());
                if (type == QLatin1String("sampler2D")) {
                    SourceData d;
                    d.mapper = new QSignalMapper;
                    d.source = 0;
                    d.name = name.toLatin1();
                    d.ownedByEffect = false;
                    m_sources.append(d);
                }
            }
        }
    }
}
