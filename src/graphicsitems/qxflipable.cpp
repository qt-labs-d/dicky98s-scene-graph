/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxflipable_p.h"
#include "qxitem_p.h"

#include <private/qdeclarativeguard_p.h>
#include <QtDeclarative/qdeclarativeinfo.h>
#include <QtGui/qgraphicstransform.h>

class QxFlipablePrivate : public QxItemPrivate
{
    Q_DECLARE_PUBLIC(QxFlipable)
public:
    QxFlipablePrivate() : current(QxFlipable::Front), front(0), back(0), backTransform(0) {}

    void updateVisibleSide();
    void setBackTransform();

    QxFlipable::Side current;
    QDeclarativeGuard<QxItem> front;
    QDeclarativeGuard<QxItem> back;

    bool wantBackXFlipped;
    bool wantBackYFlipped;
    QGraphicsRotation *backTransform;
};

QxFlipable::QxFlipable(QxItem *parent)
: QxItem(*(new QxFlipablePrivate), parent)
{
}

QxFlipable::~QxFlipable()
{
}

QxItem *QxFlipable::front()
{
    Q_D(const QxFlipable);
    return d->front;
}

void QxFlipable::setFront(QxItem *front)
{
    Q_D(QxFlipable);
    if (d->front) {
        qmlInfo(this) << tr("front is a write-once property");
        return;
    }
    d->front = front;
    d->front->setParentItem(this);
    if (Back == d->current)
        d->front->setOpacity(0.);

    emit frontChanged();
}

QxItem *QxFlipable::back()
{
    Q_D(const QxFlipable);
    return d->back;
}

void QxFlipable::setBack(QxItem *back)
{
    Q_D(QxFlipable);
    if (d->back) {
        qmlInfo(this) << tr("back is a write-once property");
        return;
    }
    d->back = back;
    d->back->setParentItem(this);
    if (Front == d->current)
        d->back->setOpacity(0.);
    connect(back, SIGNAL(widthChanged()),
            this, SLOT(retransformBack()));
    connect(back, SIGNAL(heightChanged()),
            this, SLOT(retransformBack()));

    emit backChanged();
}

void QxFlipable::retransformBack()
{
    Q_D(QxFlipable);
    if (d->current == QxFlipable::Back && d->back)
        d->setBackTransform();
}

QxFlipable::Side QxFlipable::side() const
{
    Q_D(const QxFlipable);
    const_cast<QxFlipablePrivate *>(d)->updateVisibleSide();
    return d->current;
}

void QxFlipable::transformChanged(const QTransform &newTransform, const QTransform &oldTransform)
{
    Q_UNUSED(newTransform);
    Q_UNUSED(oldTransform);
    Q_D(QxFlipable);
    d->updateVisibleSide();
}

// determination on the currently visible side of the flipable
// has to be done on the complete scene transform to give
// correct results.
void QxFlipablePrivate::updateVisibleSide()
{
    QPointF p1(0, 0);
    QPointF p2(1, 0);
    QPointF p3(1, 1);

    Q_Q(QxItem);
    QPointF scenep1 = q->mapToScene(p1);
    QPointF scenep2 = q->mapToScene(p2);
    QPointF scenep3 = q->mapToScene(p3);

    if (!parentItem) {
        return;
    }

    p1 = parentItem->mapFromScene(scenep1);
    p2 = parentItem->mapFromScene(scenep2);
    p3 = parentItem->mapFromScene(scenep3);

    qreal cross = (scenep1.x() - scenep2.x()) * (scenep3.y() - scenep2.y()) -
                  (scenep1.y() - scenep2.y()) * (scenep3.x() - scenep2.x());

    wantBackYFlipped = p1.x() >= p2.x();
    wantBackXFlipped = p2.y() >= p3.y();

    QxFlipable::Side newSide;
    if (cross > 0) {
        newSide = QxFlipable::Back;
    } else {
        newSide = QxFlipable::Front;
    }

    if (newSide != current) {
        current = newSide;
        if (current == QxFlipable::Back && back)
            setBackTransform();
        if (front)
            front->setOpacity((current==QxFlipable::Front)?1.:0.);
        if (back)
            back->setOpacity((current==QxFlipable::Back)?1.:0.);
        emit q_func()->sideChanged();
    }
}

void QxFlipablePrivate::setBackTransform()
{
    QxItemPrivate *backd = QxItemPrivate::get(back);

    if (!backTransform) {
        backTransform = new QGraphicsRotation;
        backd->m_transformations.append(backTransform);
    }

    backTransform->setOrigin(QVector3D(QPointF(back->width()/2, back->height()/2)));
    if (back->width() && wantBackYFlipped) {
        backTransform->setAxis(Qt::YAxis);
        backTransform->setAngle(180);
    }
    if (back->height() && wantBackXFlipped) {
        backTransform->setAxis(Qt::XAxis);
        backTransform->setAngle(180);
    }

    backd->updateGeometry();
}
