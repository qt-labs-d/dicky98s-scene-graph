/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXFLIPABLE_H
#define QXFLIPABLE_H

#include "qxitem.h"

#include <QtCore/QObject>
#include <QtGui/QTransform>
#include <QtGui/qvector3d.h>

class QxFlipablePrivate;
class QxFlipable : public QxItem
{
    Q_OBJECT

    Q_ENUMS(Side)
    Q_PROPERTY(QxItem *front READ front WRITE setFront NOTIFY frontChanged)
    Q_PROPERTY(QxItem *back READ back WRITE setBack NOTIFY backChanged)
    Q_PROPERTY(Side side READ side NOTIFY sideChanged)
    //### flipAxis
    //### flipRotation
public:
    QxFlipable(QxItem *parent=0);
    ~QxFlipable();

    QxItem *front();
    void setFront(QxItem *);

    QxItem *back();
    void setBack(QxItem *);

    enum Side { Front, Back };
    Side side() const;

Q_SIGNALS:
    void sideChanged();
    void backChanged();
    void frontChanged();

protected:
    virtual void transformChanged(const QTransform &newTransform, const QTransform &oldTransform);

private Q_SLOTS:
    void retransformBack();

private:
    Q_DISABLE_COPY(QxFlipable)
    Q_DECLARE_PRIVATE(QxFlipable)
};

QML_DECLARE_TYPE(QxFlipable)

#endif // QXFLIPABLE_H
