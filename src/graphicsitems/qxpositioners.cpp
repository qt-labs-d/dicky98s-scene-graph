/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxpositioners_p.h"
#include "qxpositioners_p_p.h"

#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/qdeclarativeinfo.h>
#include <QtCore/qmath.h>

#include <private/qdeclarativestate_p.h>
#include <private/qdeclarativestategroup_p.h>
#include <private/qdeclarativestateoperations_p.h>

#include <QtCore/qdebug.h>
#include <QtCore/qcoreapplication.h>

static const QxItemPrivate::ChangeTypes watchedChanges
    = QxItemPrivate::Geometry
    | QxItemPrivate::SiblingOrder
    | QxItemPrivate::Visibility
    | QxItemPrivate::Opacity
    | QxItemPrivate::Destroyed;

void QxBasePositionerPrivate::watchChanges(QxItem *other)
{
    QxItemPrivate *otherPrivate = QxItemPrivate::get(other);
    otherPrivate->addItemChangeListener(this, watchedChanges);
}

void QxBasePositionerPrivate::unwatchChanges(QxItem* other)
{
    QxItemPrivate *otherPrivate = QxItemPrivate::get(other);
    otherPrivate->removeItemChangeListener(this, watchedChanges);
}

QxBasePositioner::QxBasePositioner(PositionerType at, QxItem *parent)
    : QxItem(*(new QxBasePositionerPrivate), parent)
{
    Q_D(QxBasePositioner);
    d->init(at);
}

QxBasePositioner::QxBasePositioner(QxBasePositionerPrivate &dd, PositionerType at, QxItem *parent)
    : QxItem(dd, parent)
{
    Q_D(QxBasePositioner);
    d->init(at);
}

QxBasePositioner::~QxBasePositioner()
{
    Q_D(QxBasePositioner);
    for (int i = 0; i < positionedItems.count(); ++i)
        d->unwatchChanges(positionedItems.at(i).item);
    positionedItems.clear();
}

int QxBasePositioner::spacing() const
{
    Q_D(const QxBasePositioner);
    return d->spacing;
}

void QxBasePositioner::setSpacing(int s)
{
    Q_D(QxBasePositioner);
    if (s==d->spacing)
        return;
    d->spacing = s;
    prePositioning();
    emit spacingChanged();
}

QDeclarativeTransition *QxBasePositioner::move() const
{
    Q_D(const QxBasePositioner);
    return d->moveTransition;
}

void QxBasePositioner::setMove(QDeclarativeTransition *mt)
{
    Q_D(QxBasePositioner);
    if (mt == d->moveTransition)
        return;
    d->moveTransition = mt;
    emit moveChanged();
}

QDeclarativeTransition *QxBasePositioner::add() const
{
    Q_D(const QxBasePositioner);
    return d->addTransition;
}

void QxBasePositioner::setAdd(QDeclarativeTransition *add)
{
    Q_D(QxBasePositioner);
    if (add == d->addTransition)
        return;

    d->addTransition = add;
    emit addChanged();
}

void QxBasePositioner::componentComplete()
{
    QxItem::componentComplete();
    prePositioning();
    reportConflictingAnchors();
}

QVariant QxBasePositioner::itemChange(QGraphicsItem::GraphicsItemChange change,
                                       const QVariant &value)
{
    Q_D(QxBasePositioner);
    if (change == QGraphicsItem::ItemChildAddedChange) {
        prePositioning();
    } else if (change == QGraphicsItem::ItemChildRemovedChange) {
        QxItem* child = qobject_cast<QxItem *>(value.value<QxItem*>());
        if (child) {
            QxBasePositioner::PositionedItem posItem(child);
            int idx = positionedItems.find(posItem);
            if (idx >= 0) {
                d->unwatchChanges(child);
                positionedItems.remove(idx);
            }
            prePositioning();
        }
    }

    return QxItem::itemChange(change, value);
}

void QxBasePositioner::prePositioning()
{
    Q_D(QxBasePositioner);
    if (!isComponentComplete())
        return;

    if (d->doingPositioning)
        return;

    d->queuedPositioning = false;
    d->doingPositioning = true;
    //Need to order children by creation order modified by stacking order
    QList<QxItem *> children = childItems();
    // XXX akennedy
    // qSort(children.begin(), children.end(), d->insertionOrder);

    QPODVector<PositionedItem,8> oldItems;
    positionedItems.copyAndClear(oldItems);
    for (int ii = 0; ii < children.count(); ++ii) {
        QxItem *child = qobject_cast<QxItem *>(children.at(ii));
        if (!child)
            continue;
        PositionedItem *item = 0;
        PositionedItem posItem(child);
        int wIdx = oldItems.find(posItem);
        if (wIdx < 0) {
            d->watchChanges(child);
            positionedItems.append(posItem);
            item = &positionedItems[positionedItems.count()-1];
            item->isNew = true;
            if (child->opacity() <= 0.0 || !child->isVisible())
                item->isVisible = false;
        } else {
            item = &oldItems[wIdx];
            if (child->opacity() <= 0.0 || !child->isVisible()) {
                item->isVisible = false;
            } else if (!item->isVisible) {
                item->isVisible = true;
                item->isNew = true;
            } else {
                item->isNew = false;
            }
            positionedItems.append(*item);
        }
    }
    QSizeF contentSize;
    doPositioning(&contentSize);
    if(d->addTransition || d->moveTransition)
        finishApplyTransitions();
    d->doingPositioning = false;
    //Set implicit size to the size of its children
    setImplicitHeight(contentSize.height());
    setImplicitWidth(contentSize.width());
}

void QxBasePositioner::positionX(int x, const PositionedItem &target)
{
    Q_D(QxBasePositioner);
    if(d->type == Horizontal || d->type == Both){
        if (target.isNew) {
            if (!d->addTransition)
                target.item->setX(x);
            else
                d->addActions << QDeclarativeAction(target.item, QLatin1String("x"), QVariant(x));
        } else if (x != target.item->x()) {
            if (!d->moveTransition)
                target.item->setX(x);
            else
                d->moveActions << QDeclarativeAction(target.item, QLatin1String("x"), QVariant(x));
        }
    }
}

void QxBasePositioner::positionY(int y, const PositionedItem &target)
{
    Q_D(QxBasePositioner);
    if(d->type == Vertical || d->type == Both){
        if (target.isNew) {
            if (!d->addTransition)
                target.item->setY(y);
            else
                d->addActions << QDeclarativeAction(target.item, QLatin1String("y"), QVariant(y));
        } else if (y != target.item->y()) {
            if (!d->moveTransition)
                target.item->setY(y);
            else
                d->moveActions << QDeclarativeAction(target.item, QLatin1String("y"), QVariant(y));
        }
    }
}

void QxBasePositioner::finishApplyTransitions()
{
    Q_D(QxBasePositioner);
    // Note that if a transition is not set the transition manager will
    // apply the changes directly, in the case add/move aren't set
    d->addTransitionManager.transition(d->addActions, d->addTransition);
    d->moveTransitionManager.transition(d->moveActions, d->moveTransition);
    d->addActions.clear();
    d->moveActions.clear();
}

QxColumn::QxColumn(QxItem *parent)
: QxBasePositioner(Vertical, parent)
{
}

static inline bool isInvisible(QxItem *child)
{
    return child->opacity() == 0.0 || !child->isVisible() || !child->width() || !child->height();
}

void QxColumn::doPositioning(QSizeF *contentSize)
{
    int voffset = 0;

    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (!child.item || isInvisible(child.item))
            continue;

        if(child.item->y() != voffset)
            positionY(voffset, child);

        contentSize->setWidth(qMax(contentSize->width(), child.item->width()));

        voffset += child.item->height();
        voffset += spacing();
    }

    contentSize->setHeight(voffset - spacing());
}

void QxColumn::reportConflictingAnchors()
{
    QxBasePositionerPrivate *d = static_cast<QxBasePositionerPrivate*>(QxBasePositionerPrivate::get(this));
    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (child.item) {
            QxAnchors *anchors = QxItemPrivate::get(child.item)->_anchors;
            if (anchors) {
                QxAnchors::Anchors usedAnchors = anchors->usedAnchors();
                if (usedAnchors & QxAnchors::TopAnchor ||
                    usedAnchors & QxAnchors::BottomAnchor ||
                    usedAnchors & QxAnchors::VCenterAnchor ||
                    anchors->fill() || anchors->centerIn()) {
                    d->anchorConflict = true;
                    break;
                }
            }
        }
    }
    if (d->anchorConflict) {
        qmlInfo(this) << "Cannot specify top, bottom, verticalCenter, fill or centerIn anchors for items inside Column";
    }
}

QxRow::QxRow(QxItem *parent)
: QxBasePositioner(Horizontal, parent)
{
}

void QxRow::doPositioning(QSizeF *contentSize)
{
    int hoffset = 0;

    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (!child.item || isInvisible(child.item))
            continue;

        if(child.item->x() != hoffset)
            positionX(hoffset, child);

        contentSize->setHeight(qMax(contentSize->height(), child.item->height()));

        hoffset += child.item->width();
        hoffset += spacing();
    }

    contentSize->setWidth(hoffset - spacing());
}

void QxRow::reportConflictingAnchors()
{
    QxBasePositionerPrivate *d = static_cast<QxBasePositionerPrivate*>(QxBasePositionerPrivate::get(this));
    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (child.item) {
            QxAnchors *anchors = QxItemPrivate::get(child.item)->_anchors;
            if (anchors) {
                QxAnchors::Anchors usedAnchors = anchors->usedAnchors();
                if (usedAnchors & QxAnchors::LeftAnchor ||
                    usedAnchors & QxAnchors::RightAnchor ||
                    usedAnchors & QxAnchors::HCenterAnchor ||
                    anchors->fill() || anchors->centerIn()) {
                    d->anchorConflict = true;
                    break;
                }
            }
        }
    }
    if (d->anchorConflict)
        qmlInfo(this) << "Cannot specify left, right, horizontalCenter, fill or centerIn anchors for items inside Row";
}

QxGrid::QxGrid(QxItem *parent) :
    QxBasePositioner(Both, parent), m_rows(-1), m_columns(-1), m_flow(LeftToRight)
{
}

void QxGrid::setColumns(const int columns)
{
    if (columns == m_columns)
        return;
    m_columns = columns;
    prePositioning();
    emit columnsChanged();
}

void QxGrid::setRows(const int rows)
{
    if (rows == m_rows)
        return;
    m_rows = rows;
    prePositioning();
    emit rowsChanged();
}

QxGrid::Flow QxGrid::flow() const
{
    return m_flow;
}

void QxGrid::setFlow(Flow flow)
{
    if (m_flow != flow) {
        m_flow = flow;
        prePositioning();
        emit flowChanged();
    }
}

void QxGrid::doPositioning(QSizeF *contentSize)
{
    int c = m_columns;
    int r = m_rows;
    int numVisible = positionedItems.count();
    if (m_columns <= 0 && m_rows <= 0){
        c = 4;
        r = (numVisible+3)/4;
    } else if (m_rows <= 0){
        r = (numVisible+(m_columns-1))/m_columns;
    } else if (m_columns <= 0){
        c = (numVisible+(m_rows-1))/m_rows;
    }

    QList<int> maxColWidth;
    QList<int> maxRowHeight;
    int childIndex =0;
    if (m_flow == LeftToRight) {
        for (int i=0; i < r; i++){
            for (int j=0; j < c; j++){
                if (j==0)
                    maxRowHeight << 0;
                if (i==0)
                    maxColWidth << 0;

                if (childIndex == positionedItems.count())
                    continue;
                const PositionedItem &child = positionedItems.at(childIndex++);
                if (!child.item || isInvisible(child.item))
                    continue;
                if (child.item->width() > maxColWidth[j])
                    maxColWidth[j] = child.item->width();
                if (child.item->height() > maxRowHeight[i])
                    maxRowHeight[i] = child.item->height();
            }
        }
    } else {
        for (int j=0; j < c; j++){
            for (int i=0; i < r; i++){
                if (j==0)
                    maxRowHeight << 0;
                if (i==0)
                    maxColWidth << 0;

                if (childIndex == positionedItems.count())
                    continue;
                const PositionedItem &child = positionedItems.at(childIndex++);
                if (!child.item || isInvisible(child.item))
                    continue;
                if (child.item->width() > maxColWidth[j])
                    maxColWidth[j] = child.item->width();
                if (child.item->height() > maxRowHeight[i])
                    maxRowHeight[i] = child.item->height();
            }
        }
    }

    int xoffset=0;
    int yoffset=0;
    int curRow =0;
    int curCol =0;
    for (int i = 0; i < positionedItems.count(); ++i) {
        const PositionedItem &child = positionedItems.at(i);
        if (!child.item || isInvisible(child.item))
            continue;
        if((child.item->x()!=xoffset)||(child.item->y()!=yoffset)){
            positionX(xoffset, child);
            positionY(yoffset, child);
        }

        if (m_flow == LeftToRight) {
            contentSize->setWidth(qMax(contentSize->width(), xoffset + child.item->width()));
            contentSize->setHeight(yoffset + maxRowHeight[curRow]);

            xoffset+=maxColWidth[curCol]+spacing();
            curCol++;
            curCol%=c;
            if (!curCol){
                yoffset+=maxRowHeight[curRow]+spacing();
                xoffset=0;
                curRow++;
                if (curRow>=r)
                    break;
            }
        } else {
            contentSize->setHeight(qMax(contentSize->height(), yoffset + child.item->height()));
            contentSize->setWidth(xoffset + maxColWidth[curCol]);

            yoffset+=maxRowHeight[curRow]+spacing();
            curRow++;
            curRow%=r;
            if (!curRow){
                xoffset+=maxColWidth[curCol]+spacing();
                yoffset=0;
                curCol++;
                if (curCol>=c)
                    break;
            }
        }
    }
}

void QxGrid::reportConflictingAnchors()
{
    QxBasePositionerPrivate *d = static_cast<QxBasePositionerPrivate*>(QxBasePositionerPrivate::get(this));
    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (child.item) {
            QxAnchors *anchors = QxItemPrivate::get(child.item)->_anchors;
            if (anchors && (anchors->usedAnchors() || anchors->fill() || anchors->centerIn())) {
                d->anchorConflict = true;
                break;
            }
        }
    }
    if (d->anchorConflict)
        qmlInfo(this) << "Cannot specify anchors for items inside Grid";
}

class QxFlowPrivate : public QxBasePositionerPrivate
{
    Q_DECLARE_PUBLIC(QxFlow)

public:
    QxFlowPrivate()
        : QxBasePositionerPrivate(), flow(QxFlow::LeftToRight)
    {}

    QxFlow::Flow flow;
};

QxFlow::QxFlow(QxItem *parent)
: QxBasePositioner(*(new QxFlowPrivate), Both, parent)
{
    Q_D(QxFlow);
    // Flow layout requires relayout if its own size changes too.
    d->addItemChangeListener(d, QxItemPrivate::Geometry);
}

QxFlow::Flow QxFlow::flow() const
{
    Q_D(const QxFlow);
    return d->flow;
}

void QxFlow::setFlow(Flow flow)
{
    Q_D(QxFlow);
    if (d->flow != flow) {
        d->flow = flow;
        prePositioning();
        emit flowChanged();
    }
}

void QxFlow::doPositioning(QSizeF *contentSize)
{
    Q_D(QxFlow);

    int hoffset = 0;
    int voffset = 0;
    int linemax = 0;

    for (int i = 0; i < positionedItems.count(); ++i) {
        const PositionedItem &child = positionedItems.at(i);
        if (!child.item || isInvisible(child.item))
            continue;

        if (d->flow == LeftToRight)  {
            if (hoffset && hoffset + child.item->width() > width()) {
                hoffset = 0;
                voffset += linemax + spacing();
                linemax = 0;
            }
        } else {
            if (voffset && voffset + child.item->height() > height()) {
                voffset = 0;
                hoffset += linemax + spacing();
                linemax = 0;
            }
        }

        if(child.item->x() != hoffset || child.item->y() != voffset){
            positionX(hoffset, child);
            positionY(voffset, child);
        }

        contentSize->setWidth(qMax(contentSize->width(), hoffset + child.item->width()));
        contentSize->setHeight(qMax(contentSize->height(), voffset + child.item->height()));

        if (d->flow == LeftToRight)  {
            hoffset += child.item->width();
            hoffset += spacing();
            linemax = qMax(linemax, qCeil(child.item->height()));
        } else {
            voffset += child.item->height();
            voffset += spacing();
            linemax = qMax(linemax, qCeil(child.item->width()));
        }
    }
}

void QxFlow::reportConflictingAnchors()
{
    Q_D(QxFlow);
    for (int ii = 0; ii < positionedItems.count(); ++ii) {
        const PositionedItem &child = positionedItems.at(ii);
        if (child.item) {
            QxAnchors *anchors = QxItemPrivate::get(child.item)->_anchors;
            if (anchors && (anchors->usedAnchors() || anchors->fill() || anchors->centerIn())) {
                d->anchorConflict = true;
                break;
            }
        }
    }
    if (d->anchorConflict)
        qmlInfo(this) << "Cannot specify anchors for items inside Flow";
}
