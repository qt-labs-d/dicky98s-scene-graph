/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxstateoperations_p.h"
#include "qxitem_p.h"

#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/qdeclarativecontext.h>
#include <QtDeclarative/qdeclarativeexpression.h>
#include <QtDeclarative/qdeclarativeinfo.h>
#include <QtGui/qgraphicsitem.h>
#include <QtCore/qmath.h>
#include <QtCore/qdebug.h>

#include <private/qdeclarativeanchors_p_p.h>
#include <private/qdeclarativeguard_p.h>
#include <private/qdeclarativenullablevalue_p_p.h>
#include <private/qdeclarativecontext_p.h>
#include <private/qdeclarativeproperty_p.h>
#include <private/qdeclarativebinding_p.h>

#include <private/qobject_p.h>

class QxParentChangePrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(QxParentChange)
public:
    QxParentChangePrivate() : target(0), parent(0), origParent(0), origStackBefore(0),
        rewindParent(0), rewindStackBefore(0) {}

    QxItem *target;
    QDeclarativeGuard<QxItem> parent;
    QDeclarativeGuard<QxItem> origParent;
    QDeclarativeGuard<QxItem> origStackBefore;
    QxItem *rewindParent;
    QxItem *rewindStackBefore;

    QDeclarativeNullableValue<QDeclarativeScriptString> xString;
    QDeclarativeNullableValue<QDeclarativeScriptString> yString;
    QDeclarativeNullableValue<QDeclarativeScriptString> widthString;
    QDeclarativeNullableValue<QDeclarativeScriptString> heightString;
    QDeclarativeNullableValue<QDeclarativeScriptString> scaleString;
    QDeclarativeNullableValue<QDeclarativeScriptString> rotationString;

    void doChange(QxItem *targetParent, QxItem *stackBefore = 0);
};

void QxParentChangePrivate::doChange(QxItem *targetParent, QxItem *stackBefore)
{
    if (targetParent && target && target->parentItem()) {
        Q_Q(QxParentChange);
        bool ok;
        const QTransform &transform = target->parentItem()->itemTransform(targetParent, &ok);
        if (transform.type() >= QTransform::TxShear || !ok) {
            qmlInfo(q) << QxParentChange::tr("Unable to preserve appearance under complex transform");
            ok = false;
        }

        qreal scale = 1;
        qreal rotation = 0;
        if (ok && transform.type() != QTransform::TxRotate) {
            if (transform.m11() == transform.m22())
                scale = transform.m11();
            else {
                qmlInfo(q) << QxParentChange::tr("Unable to preserve appearance under non-uniform scale");
                ok = false;
            }
        } else if (ok && transform.type() == QTransform::TxRotate) {
            if (transform.m11() == transform.m22())
                scale = qSqrt(transform.m11()*transform.m11() + transform.m12()*transform.m12());
            else {
                qmlInfo(q) << QxParentChange::tr("Unable to preserve appearance under non-uniform scale");
                ok = false;
            }

            if (scale != 0)
                rotation = atan2(transform.m12()/scale, transform.m11()/scale) * 180/M_PI;
            else {
                qmlInfo(q) << QxParentChange::tr("Unable to preserve appearance under scale of 0");
                ok = false;
            }
        }

        const QPointF &point = transform.map(QPointF(target->x(),target->y()));
        qreal x = point.x();
        qreal y = point.y();

        // setParentItem will update the transformOriginPoint if needed
        target->setParentItem(targetParent);

        if (ok && target->transformOrigin() != QxItem::TopLeft) {
            qreal tempxt = target->transformOriginPoint().x();
            qreal tempyt = target->transformOriginPoint().y();
            QTransform t;
            t.translate(-tempxt, -tempyt);
            t.rotate(rotation);
            t.scale(scale, scale);
            t.translate(tempxt, tempyt);
            const QPointF &offset = t.map(QPointF(0,0));
            x += offset.x();
            y += offset.y();
        }

        if (ok) {
            //qDebug() << x << y << rotation << scale;
            target->setX(x);
            target->setY(y);
            target->setRotation(target->rotation() + rotation);
            target->setScale(target->scale() * scale);
        }
    } else if (target) {
        target->setParentItem(targetParent);
    }

    //restore the original stack position.
    //### if stackBefore has also been reparented this won't work
    if (stackBefore)
        target->stackBefore(stackBefore);
}

QxParentChange::QxParentChange(QObject *parent)
    : QDeclarativeStateOperation(*(new QxParentChangePrivate), parent)
{
}

QxParentChange::~QxParentChange()
{
}

QDeclarativeScriptString QxParentChange::x() const
{
    Q_D(const QxParentChange);
    return d->xString.value;
}

void QxParentChange::setX(QDeclarativeScriptString x)
{
    Q_D(QxParentChange);
    d->xString = x;
}

bool QxParentChange::xIsSet() const
{
    Q_D(const QxParentChange);
    return d->xString.isValid();
}

QDeclarativeScriptString QxParentChange::y() const
{
    Q_D(const QxParentChange);
    return d->yString.value;
}

void QxParentChange::setY(QDeclarativeScriptString y)
{
    Q_D(QxParentChange);
    d->yString = y;
}

bool QxParentChange::yIsSet() const
{
    Q_D(const QxParentChange);
    return d->yString.isValid();
}

QDeclarativeScriptString QxParentChange::width() const
{
    Q_D(const QxParentChange);
    return d->widthString.value;
}

void QxParentChange::setWidth(QDeclarativeScriptString width)
{
    Q_D(QxParentChange);
    d->widthString = width;
}

bool QxParentChange::widthIsSet() const
{
    Q_D(const QxParentChange);
    return d->widthString.isValid();
}

QDeclarativeScriptString QxParentChange::height() const
{
    Q_D(const QxParentChange);
    return d->heightString.value;
}

void QxParentChange::setHeight(QDeclarativeScriptString height)
{
    Q_D(QxParentChange);
    d->heightString = height;
}

bool QxParentChange::heightIsSet() const
{
    Q_D(const QxParentChange);
    return d->heightString.isValid();
}

QDeclarativeScriptString QxParentChange::scale() const
{
    Q_D(const QxParentChange);
    return d->scaleString.value;
}

void QxParentChange::setScale(QDeclarativeScriptString scale)
{
    Q_D(QxParentChange);
    d->scaleString = scale;
}

bool QxParentChange::scaleIsSet() const
{
    Q_D(const QxParentChange);
    return d->scaleString.isValid();
}

QDeclarativeScriptString QxParentChange::rotation() const
{
    Q_D(const QxParentChange);
    return d->rotationString.value;
}

void QxParentChange::setRotation(QDeclarativeScriptString rotation)
{
    Q_D(QxParentChange);
    d->rotationString = rotation;
}

bool QxParentChange::rotationIsSet() const
{
    Q_D(const QxParentChange);
    return d->rotationString.isValid();
}

QxItem *QxParentChange::originalParent() const
{
    Q_D(const QxParentChange);
    return d->origParent;
}

QxItem *QxParentChange::object() const
{
    Q_D(const QxParentChange);
    return d->target;
}

void QxParentChange::setObject(QxItem *target)
{
    Q_D(QxParentChange);
    d->target = target;
}

QxItem *QxParentChange::parent() const
{
    Q_D(const QxParentChange);
    return d->parent;
}

void QxParentChange::setParent(QxItem *parent)
{
    Q_D(QxParentChange);
    d->parent = parent;
}

QDeclarativeStateOperation::ActionList QxParentChange::actions()
{
    Q_D(QxParentChange);
    if (!d->target || !d->parent)
        return ActionList();

    ActionList actions;

    QDeclarativeAction a;
    a.event = this;
    actions << a;

    if (d->xString.isValid()) {
        bool ok = false;
        QString script = d->xString.value.script();
        qreal x = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction xa(d->target, QLatin1String("x"), x);
            actions << xa;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("x")));
            QDeclarativeAction xa;
            xa.property = newBinding->property();
            xa.toBinding = newBinding;
            xa.fromValue = xa.property.read();
            xa.deletableToBinding = true;
            actions << xa;
        }
    }

    if (d->yString.isValid()) {
        bool ok = false;
        QString script = d->yString.value.script();
        qreal y = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction ya(d->target, QLatin1String("y"), y);
            actions << ya;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("y")));
            QDeclarativeAction ya;
            ya.property = newBinding->property();
            ya.toBinding = newBinding;
            ya.fromValue = ya.property.read();
            ya.deletableToBinding = true;
            actions << ya;
        }
    }

    if (d->scaleString.isValid()) {
        bool ok = false;
        QString script = d->scaleString.value.script();
        qreal scale = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction sa(d->target, QLatin1String("scale"), scale);
            actions << sa;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("scale")));
            QDeclarativeAction sa;
            sa.property = newBinding->property();
            sa.toBinding = newBinding;
            sa.fromValue = sa.property.read();
            sa.deletableToBinding = true;
            actions << sa;
        }
    }

    if (d->rotationString.isValid()) {
        bool ok = false;
        QString script = d->rotationString.value.script();
        qreal rotation = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction ra(d->target, QLatin1String("rotation"), rotation);
            actions << ra;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("rotation")));
            QDeclarativeAction ra;
            ra.property = newBinding->property();
            ra.toBinding = newBinding;
            ra.fromValue = ra.property.read();
            ra.deletableToBinding = true;
            actions << ra;
        }
    }

    if (d->widthString.isValid()) {
        bool ok = false;
        QString script = d->widthString.value.script();
        qreal width = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction wa(d->target, QLatin1String("width"), width);
            actions << wa;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("width")));
            QDeclarativeAction wa;
            wa.property = newBinding->property();
            wa.toBinding = newBinding;
            wa.fromValue = wa.property.read();
            wa.deletableToBinding = true;
            actions << wa;
        }
    }

    if (d->heightString.isValid()) {
        bool ok = false;
        QString script = d->heightString.value.script();
        qreal height = script.toFloat(&ok);
        if (ok) {
            QDeclarativeAction ha(d->target, QLatin1String("height"), height);
            actions << ha;
        } else {
            QDeclarativeBinding *newBinding = new QDeclarativeBinding(script, d->target, qmlContext(this));
            newBinding->setTarget(QDeclarativeProperty(d->target, QLatin1String("height")));
            QDeclarativeAction ha;
            ha.property = newBinding->property();
            ha.toBinding = newBinding;
            ha.fromValue = ha.property.read();
            ha.deletableToBinding = true;
            actions << ha;
        }
    }

    return actions;
}

class AccessibleFxItem : public QxItem
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QxItem)
public:
    int siblingIndex() {
        // XXX akennedy
        //Q_D(QxItem);
        //return d->siblingIndex;
        return 0;
    }
};

void QxParentChange::saveOriginals()
{
    Q_D(QxParentChange);
    saveCurrentValues();
    d->origParent = d->rewindParent;
    d->origStackBefore = d->rewindStackBefore;
}

/*void QxParentChange::copyOriginals(QDeclarativeActionEvent *other)
{
    Q_D(QxParentChange);
    QxParentChange *pc = static_cast<QxParentChange*>(other);

    d->origParent = pc->d_func()->rewindParent;
    d->origStackBefore = pc->d_func()->rewindStackBefore;

    saveCurrentValues();
}*/

void QxParentChange::execute(Reason)
{
    Q_D(QxParentChange);
    d->doChange(d->parent);
}

bool QxParentChange::isReversable()
{
    return true;
}

void QxParentChange::reverse(Reason)
{
    Q_D(QxParentChange);
    d->doChange(d->origParent, d->origStackBefore);
}

QString QxParentChange::typeName() const
{
    return QLatin1String("ParentChange");
}

bool QxParentChange::override(QDeclarativeActionEvent*other)
{
    Q_D(QxParentChange);
    if (other->typeName() != QLatin1String("ParentChange"))
        return false;
    if (QxParentChange *otherPC = static_cast<QxParentChange*>(other))
        return (d->target == otherPC->object());
    return false;
}

void QxParentChange::saveCurrentValues()
{
    Q_D(QxParentChange);
    if (!d->target) {
        d->rewindParent = 0;
        d->rewindStackBefore = 0;
        return;
    }

    d->rewindParent = d->target->parentItem();
    d->rewindStackBefore = 0;

    if (!d->rewindParent)
        return;

    //try to determine the item's original stack position so we can restore it
    int siblingIndex = ((AccessibleFxItem*)d->target)->siblingIndex() + 1;
    QList<QxItem *> children = d->rewindParent->childItems();
    for (int i = 0; i < children.count(); ++i) {
        QxItem *child = qobject_cast<QxItem*>(children.at(i));
        if (!child)
            continue;
        if (((AccessibleFxItem*)child)->siblingIndex() == siblingIndex) {
            d->rewindStackBefore = child;
            break;
        }
    }
}

void QxParentChange::rewind()
{
    Q_D(QxParentChange);
    d->doChange(d->rewindParent, d->rewindStackBefore);
}

class QxAnchorSetPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(QxAnchorSet)
public:
    QxAnchorSetPrivate()
      : usedAnchors(0), resetAnchors(0), fill(0),
        centerIn(0)/*, leftMargin(0), rightMargin(0), topMargin(0), bottomMargin(0),
        margins(0), vCenterOffset(0), hCenterOffset(0), baselineOffset(0)*/
    {
    }

    QxAnchors::Anchors usedAnchors;
    QxAnchors::Anchors resetAnchors;

    QxItem *fill;
    QxItem *centerIn;

    QDeclarativeScriptString leftScript;
    QDeclarativeScriptString rightScript;
    QDeclarativeScriptString topScript;
    QDeclarativeScriptString bottomScript;
    QDeclarativeScriptString hCenterScript;
    QDeclarativeScriptString vCenterScript;
    QDeclarativeScriptString baselineScript;

    /*qreal leftMargin;
    qreal rightMargin;
    qreal topMargin;
    qreal bottomMargin;
    qreal margins;
    qreal vCenterOffset;
    qreal hCenterOffset;
    qreal baselineOffset;*/
};

QxAnchorSet::QxAnchorSet(QObject *parent)
  : QObject(*new QxAnchorSetPrivate, parent)
{
}

QxAnchorSet::~QxAnchorSet()
{
}

QDeclarativeScriptString QxAnchorSet::top() const
{
    Q_D(const QxAnchorSet);
    return d->topScript;
}

void QxAnchorSet::setTop(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::TopAnchor;
    d->topScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetTop();
}

void QxAnchorSet::resetTop()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::TopAnchor;
    d->topScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::TopAnchor;
}

QDeclarativeScriptString QxAnchorSet::bottom() const
{
    Q_D(const QxAnchorSet);
    return d->bottomScript;
}

void QxAnchorSet::setBottom(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::BottomAnchor;
    d->bottomScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetBottom();
}

void QxAnchorSet::resetBottom()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::BottomAnchor;
    d->bottomScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::BottomAnchor;
}

QDeclarativeScriptString QxAnchorSet::verticalCenter() const
{
    Q_D(const QxAnchorSet);
    return d->vCenterScript;
}

void QxAnchorSet::setVerticalCenter(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::VCenterAnchor;
    d->vCenterScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetVerticalCenter();
}

void QxAnchorSet::resetVerticalCenter()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::VCenterAnchor;
    d->vCenterScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::VCenterAnchor;
}

QDeclarativeScriptString QxAnchorSet::baseline() const
{
    Q_D(const QxAnchorSet);
    return d->baselineScript;
}

void QxAnchorSet::setBaseline(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::BaselineAnchor;
    d->baselineScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetBaseline();
}

void QxAnchorSet::resetBaseline()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::BaselineAnchor;
    d->baselineScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::BaselineAnchor;
}

QDeclarativeScriptString QxAnchorSet::left() const
{
    Q_D(const QxAnchorSet);
    return d->leftScript;
}

void QxAnchorSet::setLeft(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::LeftAnchor;
    d->leftScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetLeft();
}

void QxAnchorSet::resetLeft()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::LeftAnchor;
    d->leftScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::LeftAnchor;
}

QDeclarativeScriptString QxAnchorSet::right() const
{
    Q_D(const QxAnchorSet);
    return d->rightScript;
}

void QxAnchorSet::setRight(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::RightAnchor;
    d->rightScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetRight();
}

void QxAnchorSet::resetRight()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::RightAnchor;
    d->rightScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::RightAnchor;
}

QDeclarativeScriptString QxAnchorSet::horizontalCenter() const
{
    Q_D(const QxAnchorSet);
    return d->hCenterScript;
}

void QxAnchorSet::setHorizontalCenter(const QDeclarativeScriptString &edge)
{
    Q_D(QxAnchorSet);
    d->usedAnchors |= QxAnchors::HCenterAnchor;
    d->hCenterScript = edge;
    if (edge.script() == QLatin1String("undefined"))
        resetHorizontalCenter();
}

void QxAnchorSet::resetHorizontalCenter()
{
    Q_D(QxAnchorSet);
    d->usedAnchors &= ~QxAnchors::HCenterAnchor;
    d->hCenterScript = QDeclarativeScriptString();
    d->resetAnchors |= QxAnchors::HCenterAnchor;
}

QxItem *QxAnchorSet::fill() const
{
    Q_D(const QxAnchorSet);
    return d->fill;
}

void QxAnchorSet::setFill(QxItem *f)
{
    Q_D(QxAnchorSet);
    d->fill = f;
}

void QxAnchorSet::resetFill()
{
    setFill(0);
}

QxItem *QxAnchorSet::centerIn() const
{
    Q_D(const QxAnchorSet);
    return d->centerIn;
}

void QxAnchorSet::setCenterIn(QxItem* c)
{
    Q_D(QxAnchorSet);
    d->centerIn = c;
}

void QxAnchorSet::resetCenterIn()
{
    setCenterIn(0);
}


class QxAnchorChangesPrivate : public QObjectPrivate
{
public:
    QxAnchorChangesPrivate()
        : target(0), anchorSet(new QxAnchorSet),
          leftBinding(0), rightBinding(0), hCenterBinding(0),
          topBinding(0), bottomBinding(0), vCenterBinding(0), baselineBinding(0),
          origLeftBinding(0), origRightBinding(0), origHCenterBinding(0),
          origTopBinding(0), origBottomBinding(0), origVCenterBinding(0),
          origBaselineBinding(0)
    {

    }
    ~QxAnchorChangesPrivate() { delete anchorSet; }

    QxItem *target;
    QxAnchorSet *anchorSet;

    QDeclarativeBinding *leftBinding;
    QDeclarativeBinding *rightBinding;
    QDeclarativeBinding *hCenterBinding;
    QDeclarativeBinding *topBinding;
    QDeclarativeBinding *bottomBinding;
    QDeclarativeBinding *vCenterBinding;
    QDeclarativeBinding *baselineBinding;

    QDeclarativeAbstractBinding *origLeftBinding;
    QDeclarativeAbstractBinding *origRightBinding;
    QDeclarativeAbstractBinding *origHCenterBinding;
    QDeclarativeAbstractBinding *origTopBinding;
    QDeclarativeAbstractBinding *origBottomBinding;
    QDeclarativeAbstractBinding *origVCenterBinding;
    QDeclarativeAbstractBinding *origBaselineBinding;

    QxAnchorLine rewindLeft;
    QxAnchorLine rewindRight;
    QxAnchorLine rewindHCenter;
    QxAnchorLine rewindTop;
    QxAnchorLine rewindBottom;
    QxAnchorLine rewindVCenter;
    QxAnchorLine rewindBaseline;

    qreal fromX;
    qreal fromY;
    qreal fromWidth;
    qreal fromHeight;

    qreal toX;
    qreal toY;
    qreal toWidth;
    qreal toHeight;

    qreal rewindX;
    qreal rewindY;
    qreal rewindWidth;
    qreal rewindHeight;

    bool applyOrigLeft;
    bool applyOrigRight;
    bool applyOrigHCenter;
    bool applyOrigTop;
    bool applyOrigBottom;
    bool applyOrigVCenter;
    bool applyOrigBaseline;

    QList<QDeclarativeAbstractBinding*> oldBindings;

    QDeclarativeProperty leftProp;
    QDeclarativeProperty rightProp;
    QDeclarativeProperty hCenterProp;
    QDeclarativeProperty topProp;
    QDeclarativeProperty bottomProp;
    QDeclarativeProperty vCenterProp;
    QDeclarativeProperty baselineProp;
};

QxAnchorChanges::QxAnchorChanges(QObject *parent)
 : QDeclarativeStateOperation(*(new QxAnchorChangesPrivate), parent)
{
}

QxAnchorChanges::~QxAnchorChanges()
{
}

QxAnchorChanges::ActionList QxAnchorChanges::actions()
{
    Q_D(QxAnchorChanges);
    d->leftBinding = d->rightBinding = d->hCenterBinding = d->topBinding
                   = d->bottomBinding = d->vCenterBinding = d->baselineBinding = 0;

    d->leftProp = QDeclarativeProperty(d->target, QLatin1String("anchors.left"));
    d->rightProp = QDeclarativeProperty(d->target, QLatin1String("anchors.right"));
    d->hCenterProp = QDeclarativeProperty(d->target, QLatin1String("anchors.horizontalCenter"));
    d->topProp = QDeclarativeProperty(d->target, QLatin1String("anchors.top"));
    d->bottomProp = QDeclarativeProperty(d->target, QLatin1String("anchors.bottom"));
    d->vCenterProp = QDeclarativeProperty(d->target, QLatin1String("anchors.verticalCenter"));
    d->baselineProp = QDeclarativeProperty(d->target, QLatin1String("anchors.baseline"));

    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::LeftAnchor) {
        d->leftBinding = new QDeclarativeBinding(d->anchorSet->d_func()->leftScript.script(), d->target, qmlContext(this));
        d->leftBinding->setTarget(d->leftProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::RightAnchor) {
        d->rightBinding = new QDeclarativeBinding(d->anchorSet->d_func()->rightScript.script(), d->target, qmlContext(this));
        d->rightBinding->setTarget(d->rightProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::HCenterAnchor) {
        d->hCenterBinding = new QDeclarativeBinding(d->anchorSet->d_func()->hCenterScript.script(), d->target, qmlContext(this));
        d->hCenterBinding->setTarget(d->hCenterProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::TopAnchor) {
        d->topBinding = new QDeclarativeBinding(d->anchorSet->d_func()->topScript.script(), d->target, qmlContext(this));
        d->topBinding->setTarget(d->topProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::BottomAnchor) {
        d->bottomBinding = new QDeclarativeBinding(d->anchorSet->d_func()->bottomScript.script(), d->target, qmlContext(this));
        d->bottomBinding->setTarget(d->bottomProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::VCenterAnchor) {
        d->vCenterBinding = new QDeclarativeBinding(d->anchorSet->d_func()->vCenterScript.script(), d->target, qmlContext(this));
        d->vCenterBinding->setTarget(d->vCenterProp);
    }
    if (d->anchorSet->d_func()->usedAnchors & QxAnchors::BaselineAnchor) {
        d->baselineBinding = new QDeclarativeBinding(d->anchorSet->d_func()->baselineScript.script(), d->target, qmlContext(this));
        d->baselineBinding->setTarget(d->baselineProp);
    }

    QDeclarativeAction a;
    a.event = this;
    return ActionList() << a;
}

QxAnchorSet *QxAnchorChanges::anchors()
{
    Q_D(QxAnchorChanges);
    return d->anchorSet;
}

QxItem *QxAnchorChanges::object() const
{
    Q_D(const QxAnchorChanges);
    return d->target;
}

void QxAnchorChanges::setObject(QxItem *target)
{
    Q_D(QxAnchorChanges);
    d->target = target;
}

void QxAnchorChanges::execute(Reason reason)
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    QxItemPrivate *targetPrivate = QxItemPrivate::get(d->target);
    //incorporate any needed "reverts"
    if (d->applyOrigLeft) {
        if (!d->origLeftBinding)
            targetPrivate->anchors()->resetLeft();
        QDeclarativePropertyPrivate::setBinding(d->leftProp, d->origLeftBinding);
    }
    if (d->applyOrigRight) {
        if (!d->origRightBinding)
            targetPrivate->anchors()->resetRight();
        QDeclarativePropertyPrivate::setBinding(d->rightProp, d->origRightBinding);
    }
    if (d->applyOrigHCenter) {
        if (!d->origHCenterBinding)
            targetPrivate->anchors()->resetHorizontalCenter();
        QDeclarativePropertyPrivate::setBinding(d->hCenterProp, d->origHCenterBinding);
    }
    if (d->applyOrigTop) {
        if (!d->origTopBinding)
            targetPrivate->anchors()->resetTop();
        QDeclarativePropertyPrivate::setBinding(d->topProp, d->origTopBinding);
    }
    if (d->applyOrigBottom) {
        if (!d->origBottomBinding)
            targetPrivate->anchors()->resetBottom();
        QDeclarativePropertyPrivate::setBinding(d->bottomProp, d->origBottomBinding);
    }
    if (d->applyOrigVCenter) {
        if (!d->origVCenterBinding)
            targetPrivate->anchors()->resetVerticalCenter();
        QDeclarativePropertyPrivate::setBinding(d->vCenterProp, d->origVCenterBinding);
    }
    if (d->applyOrigBaseline) {
        if (!d->origBaselineBinding)
            targetPrivate->anchors()->resetBaseline();
        QDeclarativePropertyPrivate::setBinding(d->baselineProp, d->origBaselineBinding);
    }

    //destroy old bindings
    if (reason == ActualChange) {
        for (int i = 0; i < d->oldBindings.size(); ++i) {
            QDeclarativeAbstractBinding *binding = d->oldBindings.at(i);
            if (binding)
                binding->destroy();
        }
        d->oldBindings.clear();
    }

    //reset any anchors that have been specified as "undefined"
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::LeftAnchor) {
        targetPrivate->anchors()->resetLeft();
        QDeclarativePropertyPrivate::setBinding(d->leftProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::RightAnchor) {
        targetPrivate->anchors()->resetRight();
        QDeclarativePropertyPrivate::setBinding(d->rightProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::HCenterAnchor) {
        targetPrivate->anchors()->resetHorizontalCenter();
        QDeclarativePropertyPrivate::setBinding(d->hCenterProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::TopAnchor) {
        targetPrivate->anchors()->resetTop();
        QDeclarativePropertyPrivate::setBinding(d->topProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::BottomAnchor) {
        targetPrivate->anchors()->resetBottom();
        QDeclarativePropertyPrivate::setBinding(d->bottomProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::VCenterAnchor) {
        targetPrivate->anchors()->resetVerticalCenter();
        QDeclarativePropertyPrivate::setBinding(d->vCenterProp, 0);
    }
    if (d->anchorSet->d_func()->resetAnchors & QxAnchors::BaselineAnchor) {
        targetPrivate->anchors()->resetBaseline();
        QDeclarativePropertyPrivate::setBinding(d->baselineProp, 0);
    }

    //set any anchors that have been specified
    if (d->leftBinding)
        QDeclarativePropertyPrivate::setBinding(d->leftBinding->property(), d->leftBinding);
    if (d->rightBinding)
        QDeclarativePropertyPrivate::setBinding(d->rightBinding->property(), d->rightBinding);
    if (d->hCenterBinding)
        QDeclarativePropertyPrivate::setBinding(d->hCenterBinding->property(), d->hCenterBinding);
    if (d->topBinding)
        QDeclarativePropertyPrivate::setBinding(d->topBinding->property(), d->topBinding);
    if (d->bottomBinding)
        QDeclarativePropertyPrivate::setBinding(d->bottomBinding->property(), d->bottomBinding);
    if (d->vCenterBinding)
        QDeclarativePropertyPrivate::setBinding(d->vCenterBinding->property(), d->vCenterBinding);
    if (d->baselineBinding)
        QDeclarativePropertyPrivate::setBinding(d->baselineBinding->property(), d->baselineBinding);
}

bool QxAnchorChanges::isReversable()
{
    return true;
}

void QxAnchorChanges::reverse(Reason reason)
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    QxItemPrivate *targetPrivate = QxItemPrivate::get(d->target);
    //reset any anchors set by the state
    if (d->leftBinding) {
        targetPrivate->anchors()->resetLeft();
        QDeclarativePropertyPrivate::setBinding(d->leftBinding->property(), 0);
        if (reason == ActualChange) {
            d->leftBinding->destroy(); d->leftBinding = 0;
        }
    }
    if (d->rightBinding) {
        targetPrivate->anchors()->resetRight();
        QDeclarativePropertyPrivate::setBinding(d->rightBinding->property(), 0);
        if (reason == ActualChange) {
            d->rightBinding->destroy(); d->rightBinding = 0;
        }
    }
    if (d->hCenterBinding) {
        targetPrivate->anchors()->resetHorizontalCenter();
        QDeclarativePropertyPrivate::setBinding(d->hCenterBinding->property(), 0);
        if (reason == ActualChange) {
            d->hCenterBinding->destroy(); d->hCenterBinding = 0;
        }
    }
    if (d->topBinding) {
        targetPrivate->anchors()->resetTop();
        QDeclarativePropertyPrivate::setBinding(d->topBinding->property(), 0);
        if (reason == ActualChange) {
            d->topBinding->destroy(); d->topBinding = 0;
        }
    }
    if (d->bottomBinding) {
        targetPrivate->anchors()->resetBottom();
        QDeclarativePropertyPrivate::setBinding(d->bottomBinding->property(), 0);
        if (reason == ActualChange) {
            d->bottomBinding->destroy(); d->bottomBinding = 0;
        }
    }
    if (d->vCenterBinding) {
        targetPrivate->anchors()->resetVerticalCenter();
        QDeclarativePropertyPrivate::setBinding(d->vCenterBinding->property(), 0);
        if (reason == ActualChange) {
            d->vCenterBinding->destroy(); d->vCenterBinding = 0;
        }
    }
    if (d->baselineBinding) {
        targetPrivate->anchors()->resetBaseline();
        QDeclarativePropertyPrivate::setBinding(d->baselineBinding->property(), 0);
        if (reason == ActualChange) {
            d->baselineBinding->destroy(); d->baselineBinding = 0;
        }
    }

    //restore previous anchors
    if (d->origLeftBinding)
        QDeclarativePropertyPrivate::setBinding(d->leftProp, d->origLeftBinding);
    if (d->origRightBinding)
        QDeclarativePropertyPrivate::setBinding(d->rightProp, d->origRightBinding);
    if (d->origHCenterBinding)
        QDeclarativePropertyPrivate::setBinding(d->hCenterProp, d->origHCenterBinding);
    if (d->origTopBinding)
        QDeclarativePropertyPrivate::setBinding(d->topProp, d->origTopBinding);
    if (d->origBottomBinding)
        QDeclarativePropertyPrivate::setBinding(d->bottomProp, d->origBottomBinding);
    if (d->origVCenterBinding)
        QDeclarativePropertyPrivate::setBinding(d->vCenterProp, d->origVCenterBinding);
    if (d->origBaselineBinding)
        QDeclarativePropertyPrivate::setBinding(d->baselineProp, d->origBaselineBinding);
}

QString QxAnchorChanges::typeName() const
{
    return QLatin1String("AnchorChanges");
}

QList<QDeclarativeAction> QxAnchorChanges::additionalActions()
{
    Q_D(QxAnchorChanges);
    QList<QDeclarativeAction> extra;

    QxAnchors::Anchors combined = d->anchorSet->d_func()->usedAnchors | d->anchorSet->d_func()->resetAnchors;
    bool hChange = combined & QxAnchors::Horizontal_Mask;
    bool vChange = combined & QxAnchors::Vertical_Mask;

    if (d->target) {
        QDeclarativeAction a;
        if (hChange && d->fromX != d->toX) {
            a.property = QDeclarativeProperty(d->target, QLatin1String("x"));
            a.toValue = d->toX;
            extra << a;
        }
        if (vChange && d->fromY != d->toY) {
            a.property = QDeclarativeProperty(d->target, QLatin1String("y"));
            a.toValue = d->toY;
            extra << a;
        }
        if (hChange && d->fromWidth != d->toWidth) {
            a.property = QDeclarativeProperty(d->target, QLatin1String("width"));
            a.toValue = d->toWidth;
            extra << a;
        }
        if (vChange && d->fromHeight != d->toHeight) {
            a.property = QDeclarativeProperty(d->target, QLatin1String("height"));
            a.toValue = d->toHeight;
            extra << a;
        }
    }

    return extra;
}

bool QxAnchorChanges::changesBindings()
{
    return true;
}

void QxAnchorChanges::saveOriginals()
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    d->origLeftBinding = QDeclarativePropertyPrivate::binding(d->leftProp);
    d->origRightBinding = QDeclarativePropertyPrivate::binding(d->rightProp);
    d->origHCenterBinding = QDeclarativePropertyPrivate::binding(d->hCenterProp);
    d->origTopBinding = QDeclarativePropertyPrivate::binding(d->topProp);
    d->origBottomBinding = QDeclarativePropertyPrivate::binding(d->bottomProp);
    d->origVCenterBinding = QDeclarativePropertyPrivate::binding(d->vCenterProp);
    d->origBaselineBinding = QDeclarativePropertyPrivate::binding(d->baselineProp);

    d->applyOrigLeft = d->applyOrigRight = d->applyOrigHCenter = d->applyOrigTop
      = d->applyOrigBottom = d->applyOrigVCenter = d->applyOrigBaseline = false;

    saveCurrentValues();
}

void QxAnchorChanges::copyOriginals(QDeclarativeActionEvent *other)
{
    Q_D(QxAnchorChanges);
    QxAnchorChanges *ac = static_cast<QxAnchorChanges*>(other);
    QxAnchorChangesPrivate *acp = ac->d_func();

    QxAnchors::Anchors combined = acp->anchorSet->d_func()->usedAnchors |
                                            acp->anchorSet->d_func()->resetAnchors;

    //probably also need to revert some things
    d->applyOrigLeft = (combined & QxAnchors::LeftAnchor);
    d->applyOrigRight = (combined & QxAnchors::RightAnchor);
    d->applyOrigHCenter = (combined & QxAnchors::HCenterAnchor);
    d->applyOrigTop = (combined & QxAnchors::TopAnchor);
    d->applyOrigBottom = (combined & QxAnchors::BottomAnchor);
    d->applyOrigVCenter = (combined & QxAnchors::VCenterAnchor);
    d->applyOrigBaseline = (combined & QxAnchors::BaselineAnchor);

    d->origLeftBinding = acp->origLeftBinding;
    d->origRightBinding = acp->origRightBinding;
    d->origHCenterBinding = acp->origHCenterBinding;
    d->origTopBinding = acp->origTopBinding;
    d->origBottomBinding = acp->origBottomBinding;
    d->origVCenterBinding = acp->origVCenterBinding;
    d->origBaselineBinding = acp->origBaselineBinding;

    d->oldBindings.clear();
    d->oldBindings << acp->leftBinding << acp->rightBinding << acp->hCenterBinding
                << acp->topBinding << acp->bottomBinding << acp->baselineBinding;

    saveCurrentValues();
}

void QxAnchorChanges::clearBindings()
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    d->fromX = d->target->x();
    d->fromY = d->target->y();
    d->fromWidth = d->target->width();
    d->fromHeight = d->target->height();

    QxItemPrivate *targetPrivate = QxItemPrivate::get(d->target);
    //reset any anchors with corresponding reverts
    //reset any anchors that have been specified as "undefined"
    //reset any anchors that we'll be setting in the state
    QxAnchors::Anchors combined = d->anchorSet->d_func()->resetAnchors |
                                            d->anchorSet->d_func()->usedAnchors;
    if (d->applyOrigLeft || (combined & QxAnchors::LeftAnchor)) {
        targetPrivate->anchors()->resetLeft();
        QDeclarativePropertyPrivate::setBinding(d->leftProp, 0);
    }
    if (d->applyOrigRight || (combined & QxAnchors::RightAnchor)) {
        targetPrivate->anchors()->resetRight();
        QDeclarativePropertyPrivate::setBinding(d->rightProp, 0);
    }
    if (d->applyOrigHCenter || (combined & QxAnchors::HCenterAnchor)) {
        targetPrivate->anchors()->resetHorizontalCenter();
        QDeclarativePropertyPrivate::setBinding(d->hCenterProp, 0);
    }
    if (d->applyOrigTop || (combined & QxAnchors::TopAnchor)) {
        targetPrivate->anchors()->resetTop();
        QDeclarativePropertyPrivate::setBinding(d->topProp, 0);
    }
    if (d->applyOrigBottom || (combined & QxAnchors::BottomAnchor)) {
        targetPrivate->anchors()->resetBottom();
        QDeclarativePropertyPrivate::setBinding(d->bottomProp, 0);
    }
    if (d->applyOrigVCenter || (combined & QxAnchors::VCenterAnchor)) {
        targetPrivate->anchors()->resetVerticalCenter();
        QDeclarativePropertyPrivate::setBinding(d->vCenterProp, 0);
    }
    if (d->applyOrigBaseline || (combined & QxAnchors::BaselineAnchor)) {
        targetPrivate->anchors()->resetBaseline();
        QDeclarativePropertyPrivate::setBinding(d->baselineProp, 0);
    }
}

bool QxAnchorChanges::override(QDeclarativeActionEvent*other)
{
    if (other->typeName() != QLatin1String("AnchorChanges"))
        return false;
    if (static_cast<QDeclarativeActionEvent*>(this) == other)
        return true;
    if (static_cast<QxAnchorChanges*>(other)->object() == object())
        return true;
    return false;
}

void QxAnchorChanges::rewind()
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    QxItemPrivate *targetPrivate = QxItemPrivate::get(d->target);
    //restore previous anchors
    if (d->rewindLeft.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setLeft(d->rewindLeft);
    if (d->rewindRight.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setRight(d->rewindRight);
    if (d->rewindHCenter.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setHorizontalCenter(d->rewindHCenter);
    if (d->rewindTop.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setTop(d->rewindTop);
    if (d->rewindBottom.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setBottom(d->rewindBottom);
    if (d->rewindVCenter.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setVerticalCenter(d->rewindVCenter);
    if (d->rewindBaseline.anchorLine != QxAnchorLine::Invalid)
        targetPrivate->anchors()->setBaseline(d->rewindBaseline);

    d->target->setX(d->rewindX);
    d->target->setY(d->rewindY);
    d->target->setWidth(d->rewindWidth);
    d->target->setHeight(d->rewindHeight);
}

void QxAnchorChanges::saveCurrentValues()
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    QxItemPrivate *targetPrivate = QxItemPrivate::get(d->target);
    d->rewindLeft = targetPrivate->anchors()->left();
    d->rewindRight = targetPrivate->anchors()->right();
    d->rewindHCenter = targetPrivate->anchors()->horizontalCenter();
    d->rewindTop = targetPrivate->anchors()->top();
    d->rewindBottom = targetPrivate->anchors()->bottom();
    d->rewindVCenter = targetPrivate->anchors()->verticalCenter();
    d->rewindBaseline = targetPrivate->anchors()->baseline();

    d->rewindX = d->target->x();
    d->rewindY = d->target->y();
    d->rewindWidth = d->target->width();
    d->rewindHeight = d->target->height();
}

void QxAnchorChanges::saveTargetValues()
{
    Q_D(QxAnchorChanges);
    if (!d->target)
        return;

    d->toX = d->target->x();
    d->toY = d->target->y();
    d->toWidth = d->target->width();
    d->toHeight = d->target->height();
}

#include <qxstateoperations.moc>
#include <moc_qxstateoperations_p.cpp>

