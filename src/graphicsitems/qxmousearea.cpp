/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxmousearea_p.h"
#include "qxmousearea_p_p.h"
#include "qxevents_p.h"

#include <QtGui/qapplication.h>

static const int PressAndHoldDelay = 800;

QxDrag::QxDrag(QObject *parent)
: QObject(parent), _target(0), _axis(XandYAxis), _xmin(0), _xmax(0), _ymin(0), _ymax(0),
_active(false)
{
}

QxDrag::~QxDrag()
{
}

QxItem *QxDrag::target() const
{
    return _target;
}

void QxDrag::setTarget(QxItem *t)
{
    if (_target == t)
        return;
    _target = t;
    emit targetChanged();
}

void QxDrag::resetTarget()
{
    if (!_target)
        return;
    _target = 0;
    emit targetChanged();
}

QxDrag::Axis QxDrag::axis() const
{
    return _axis;
}

void QxDrag::setAxis(QxDrag::Axis a)
{
    if (_axis == a)
        return;
    _axis = a;
    emit axisChanged();
}

qreal QxDrag::xmin() const
{
    return _xmin;
}

void QxDrag::setXmin(qreal m)
{
    if (_xmin == m)
        return;
    _xmin = m;
    emit minimumXChanged();
}

qreal QxDrag::xmax() const
{
    return _xmax;
}

void QxDrag::setXmax(qreal m)
{
    if (_xmax == m)
        return;
    _xmax = m;
    emit maximumXChanged();
}

qreal QxDrag::ymin() const
{
    return _ymin;
}

void QxDrag::setYmin(qreal m)
{
    if (_ymin == m)
        return;
    _ymin = m;
    emit minimumYChanged();
}

qreal QxDrag::ymax() const
{
    return _ymax;
}

void QxDrag::setYmax(qreal m)
{
    if (_ymax == m)
        return;
    _ymax = m;
    emit maximumYChanged();
}

bool QxDrag::active() const
{
    return _active;
}

void QxDrag::setActive(bool drag)
{
    if (_active == drag)
        return;
    _active = drag;
    emit activeChanged();
}

QxMouseAreaPrivate::~QxMouseAreaPrivate()
{
    delete drag;
}


/*!
    \qmlclass MouseArea QxMouseArea
    \since 4.7
    \brief The MouseArea item enables simple mouse handling.
    \inherits Item

    A MouseArea is typically used in conjunction with a visible item,
    where the MouseArea effectively 'proxies' mouse handling for that
    item. For example, we can put a MouseArea in a Rectangle that changes
    the Rectangle color to red when clicked:
    \snippet doc/src/snippets/declarative/mouseregion.qml 0

    Many MouseArea signals pass a \l {MouseEvent}{mouse} parameter that contains
    additional information about the mouse event, such as the position, button,
    and any key modifiers.

    Below we have the previous
    example extended so as to give a different color when you right click.
    \snippet doc/src/snippets/declarative/mouseregion.qml 1

    For basic key handling, see the \l {Keys}{Keys attached property}.

    MouseArea is an invisible item: it is never painted.

    \sa MouseEvent
*/

/*!
    \qmlsignal MouseArea::onEntered()

    This handler is called when the mouse enters the mouse area.

    By default the onEntered handler is only called while a button is
    pressed. Setting hoverEnabled to true enables handling of
    onEntered when no mouse button is pressed.

    \sa hoverEnabled
*/

/*!
    \qmlsignal MouseArea::onExited()

    This handler is called when the mouse exists the mouse area.

    By default the onExited handler is only called while a button is
    pressed. Setting hoverEnabled to true enables handling of
    onExited when no mouse button is pressed.

    \sa hoverEnabled
*/

/*!
    \qmlsignal MouseArea::onPositionChanged(MouseEvent mouse)

    This handler is called when the mouse position changes.

    The \l {MouseEvent}{mouse} parameter provides information about the mouse, including the x and y
    position, and any buttons currently pressed.

    The \e accepted property of the MouseEvent parameter is ignored in this handler.

    By default the onPositionChanged handler is only called while a button is
    pressed.  Setting hoverEnabled to true enables handling of
    onPositionChanged when no mouse button is pressed.
*/

/*!
    \qmlsignal MouseArea::onClicked(mouse)

    This handler is called when there is a click. A click is defined as a press followed by a release,
    both inside the MouseArea (pressing, moving outside the MouseArea, and then moving back inside and
    releasing is also considered a click).

    The \l {MouseEvent}{mouse} parameter provides information about the click, including the x and y
    position of the release of the click, and whether the click wasHeld.

    The \e accepted property of the MouseEvent parameter is ignored in this handler.
*/

/*!
    \qmlsignal MouseArea::onPressed(mouse)

    This handler is called when there is a press.
    The \l {MouseEvent}{mouse} parameter provides information about the press, including the x and y
    position and which button was pressed.

    The \e accepted property of the MouseEvent parameter determines whether this MouseArea
    will handle the press and all future mouse events until release.  The default is to accept
    the event and not allow other MouseArea beneath this one to handle the event.  If \e accepted
    is set to false, no further events will be sent to this MouseArea until the button is next
    pressed.
*/

/*!
    \qmlsignal MouseArea::onReleased(mouse)

    This handler is called when there is a release.
    The \l {MouseEvent}{mouse} parameter provides information about the click, including the x and y
    position of the release of the click, and whether the click wasHeld.

    The \e accepted property of the MouseEvent parameter is ignored in this handler.
*/

/*!
    \qmlsignal MouseArea::onPressAndHold(mouse)

    This handler is called when there is a long press (currently 800ms).
    The \l {MouseEvent}{mouse} parameter provides information about the press, including the x and y
    position of the press, and which button is pressed.

    The \e accepted property of the MouseEvent parameter is ignored in this handler.
*/

/*!
    \qmlsignal MouseArea::onDoubleClicked(mouse)

    This handler is called when there is a double-click (a press followed by a release followed by a press).
    The \l {MouseEvent}{mouse} parameter provides information about the click, including the x and y
    position of the release of the click, and whether the click wasHeld.

    The \e accepted property of the MouseEvent parameter is ignored in this handler.
*/

/*!
    \qmlsignal MouseArea::onCanceled()

    This handler is called when the mouse events are canceled, either because the event was not accepted or
    another element stole the mouse event handling. This signal is for advanced users, it's useful in case there
    is more than one mouse areas handling input, or when there is a mouse area inside a flickable. In the latter
    case, if you do some logic on pressed and then start dragging, the flickable will steal the mouse handling
    from the mouse area. In these cases, to reset the logic when there is no mouse handling anymore, you should
    use onCanceled, in addition to onReleased.
*/

/*!
    \internal
    \class QxMouseArea
    \brief The QxMouseArea class provides a simple mouse handling abstraction for use within Qml.

    All QDeclarativeItem derived classes can do mouse handling but the QxMouseArea class exposes mouse
    handling data as properties and tracks flicking and dragging of the mouse.

    A QxMouseArea object can be instantiated in Qml using the tag \l MouseArea.
 */
QxMouseArea::QxMouseArea(QxItem *parent)
: QxItem(*(new QxMouseAreaPrivate), parent)
{
    Q_D(QxMouseArea);
    d->init();
}

QxMouseArea::~QxMouseArea()
{
}

/*!
    \qmlproperty real MouseArea::mouseX
    \qmlproperty real MouseArea::mouseY
    These properties hold the coordinates of the mouse.

    If the hoverEnabled property is false then these properties will only be valid
    while a button is pressed, and will remain valid as long as the button is held
    even if the mouse is moved outside the area.

    If hoverEnabled is true then these properties will be valid:
    \list
        \i when no button is pressed, but the mouse is within the MouseArea (containsMouse is true).
        \i if a button is pressed and held, even if it has since moved out of the area.
    \endlist

    The coordinates are relative to the MouseArea.
*/
qreal QxMouseArea::mouseX() const
{
    Q_D(const QxMouseArea);
    return d->lastPos.x();
}

qreal QxMouseArea::mouseY() const
{
    Q_D(const QxMouseArea);
    return d->lastPos.y();
}

/*!
    \qmlproperty bool MouseArea::enabled
    This property holds whether the item accepts mouse events.
*/
bool QxMouseArea::isEnabled() const
{
    Q_D(const QxMouseArea);
    return d->absorb;
}

void QxMouseArea::setEnabled(bool a)
{
    Q_D(QxMouseArea);
    if (a != d->absorb) {
        d->absorb = a;
        emit enabledChanged();
    }
}
/*!
    \qmlproperty MouseButtons MouseArea::pressedButtons
    This property holds the mouse buttons currently pressed.

    It contains a bitwise combination of:
    \list
    \o Qt.LeftButton
    \o Qt.RightButton
    \o Qt.MiddleButton
    \endlist

    The code below displays "right" when the right mouse buttons is pressed:
    \code
    Text {
        text: mr.pressedButtons & Qt.RightButton ? "right" : ""
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        MouseArea {
            id: mr
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            anchors.fill: parent
        }
    }
    \endcode

    \sa acceptedButtons
*/
Qt::MouseButtons QxMouseArea::pressedButtons() const
{
    Q_D(const QxMouseArea);
    return d->lastButtons;
}

void QxMouseArea::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxMouseArea);
    d->moved = false;
    if (!d->absorb) {
        QxItem::mousePressEvent(event);
    } else {
        d->longPress = false;
        d->saveEvent(event);
        if (d->drag) {
            d->dragX = drag()->axis() & QxDrag::XAxis;
            d->dragY = drag()->axis() & QxDrag::YAxis;
        }
        if (d->drag)
            d->drag->setActive(false);
        setHovered(true);
        d->startScene = event->scenePos();
        // we should only start timer if pressAndHold is connected to.
        if (d->isConnected("pressAndHold(QxMouseEvent*)"))
            d->pressAndHoldTimer.start(PressAndHoldDelay, this);
        setKeepMouseGrab(false);
        event->setAccepted(setPressed(true));
    }
}

void QxMouseArea::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb) {
        QxItem::mouseMoveEvent(event);
        return;
    }

    d->saveEvent(event);

    // ### we could skip this if these signals aren't used
    bool contains = boundingRect().contains(d->lastPos);
    if (d->hovered && !contains)
        setHovered(false);
    else if (!d->hovered && contains)
        setHovered(true);

    if (d->drag && d->drag->target()) {
        if (!d->moved) {
            d->startX = drag()->target()->x();
            d->startY = drag()->target()->y();
        }

        QPointF startLocalPos;
        QPointF curLocalPos;
        if (drag()->target()->parent()) {
            startLocalPos = drag()->target()->parentItem()->mapFromScene(d->startScene);
            curLocalPos = drag()->target()->parentItem()->mapFromScene(event->scenePos());
        } else {
            startLocalPos = d->startScene;
            curLocalPos = event->scenePos();
        }

        const int dragThreshold = QApplication::startDragDistance();
        qreal dx = qAbs(curLocalPos.x() - startLocalPos.x());
        qreal dy = qAbs(curLocalPos.y() - startLocalPos.y());
        if ((d->dragX && !(dx < dragThreshold)) || (d->dragY && !(dy < dragThreshold)))
            d->drag->setActive(true);
        if (!keepMouseGrab()) {
            if ((!d->dragY && dy < dragThreshold && d->dragX && dx > dragThreshold)
                || (!d->dragX && dx < dragThreshold && d->dragY && dy > dragThreshold)
                || (d->dragX && d->dragY)) {
                setKeepMouseGrab(true);
            }
        }

        if (d->dragX && d->drag->active()) {
            qreal x = (curLocalPos.x() - startLocalPos.x()) + d->startX;
            if (x < drag()->xmin())
                x = drag()->xmin();
            else if (x > drag()->xmax())
                x = drag()->xmax();
            drag()->target()->setX(x);
        }
        if (d->dragY && d->drag->active()) {
            qreal y = (curLocalPos.y() - startLocalPos.y()) + d->startY;
            if (y < drag()->ymin())
                y = drag()->ymin();
            else if (y > drag()->ymax())
                y = drag()->ymax();
            drag()->target()->setY(y);
        }
        d->moved = true;
    }
    QxMouseEvent me(d->lastPos.x(), d->lastPos.y(), d->lastButton, d->lastButtons, d->lastModifiers, false, d->longPress);
    emit mousePositionChanged(&me);
    me.setX(d->lastPos.x());
    me.setY(d->lastPos.y());
    emit positionChanged(&me);
}


void QxMouseArea::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb) {
        QxItem::mouseReleaseEvent(event);
    } else {
        d->saveEvent(event);
        setPressed(false);
        if (d->drag)
            d->drag->setActive(false);
        // If we don't accept hover, we need to reset containsMouse.
        if (!acceptHoverEvents())
            setHovered(false);
        setKeepMouseGrab(false);
    }
}

void QxMouseArea::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb) {
        QxItem::mouseDoubleClickEvent(event);
    } else {
        QxItem::mouseDoubleClickEvent(event);
        if (event->isAccepted()) {
            // Only deliver the event if we have accepted the press.
            d->saveEvent(event);
            QxMouseEvent me(d->lastPos.x(), d->lastPos.y(), d->lastButton, d->lastButtons, d->lastModifiers, true, false);
            emit this->doubleClicked(&me);
        }
    }
}

void QxMouseArea::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb)
        QxItem::hoverEnterEvent(event);
    else
        setHovered(true);
}

void QxMouseArea::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb) {
        QxItem::hoverEnterEvent(event);
    } else {
        d->lastPos = event->pos();
        QxMouseEvent me(d->lastPos.x(), d->lastPos.y(), Qt::NoButton, d->lastButtons, d->lastModifiers, false, d->longPress);
        emit mousePositionChanged(&me);
        me.setX(d->lastPos.x());
        me.setY(d->lastPos.y());
        emit positionChanged(&me);
    }
}

void QxMouseArea::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_D(QxMouseArea);
    if (!d->absorb)
        QxItem::hoverLeaveEvent(event);
    else
        setHovered(false);
}

bool QxMouseArea::sceneEvent(QEvent *event)
{
    bool rv = QxItem::sceneEvent(event);
    if (event->type() == QEvent::UngrabMouse) {
        Q_D(QxMouseArea);
        if (d->pressed) {
            // if our mouse grab has been removed (probably by Flickable), fix our
            // state
            d->pressed = false;
            setKeepMouseGrab(false);
            emit canceled();
            emit pressedChanged();
            if (d->hovered) {
                d->hovered = false;
                emit hoveredChanged();
            }
        }
    }
    return rv;
}

void QxMouseArea::timerEvent(QTimerEvent *event)
{
    Q_D(QxMouseArea);
    if (event->timerId() == d->pressAndHoldTimer.timerId()) {
        d->pressAndHoldTimer.stop();
        bool dragged = d->drag && d->drag->active();
        if (d->pressed && dragged == false && d->hovered == true) {
            d->longPress = true;
            QxMouseEvent me(d->lastPos.x(), d->lastPos.y(), d->lastButton, d->lastButtons, d->lastModifiers, false, d->longPress);
            emit pressAndHold(&me);
        }
    }
}

void QxMouseArea::geometryChanged(const QRectF &newGeometry,
                                            const QRectF &oldGeometry)
{
    Q_D(QxMouseArea);
    QxItem::geometryChanged(newGeometry, oldGeometry);

    if (d->lastScenePos.isNull)
        d->lastScenePos = mapToScene(d->lastPos);
    else if (newGeometry.x() != oldGeometry.x() || newGeometry.y() != oldGeometry.y())
        d->lastPos = mapFromScene(d->lastScenePos);
}

/*! \internal */
QVariant QxMouseArea::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    Q_UNUSED(change)
    Q_UNUSED(value)
    // XXX akennedy
    /*
    Q_D(QxMouseArea);
    switch (change) {
    case ItemVisibleHasChanged:
        if (acceptHoverEvents() && d->hovered != (isVisible() && isUnderMouse()))
            setHovered(!d->hovered);
        break;
    default:
        break;
    }

    return QDeclarativeItem::itemChange(change, value);
    */
    return QVariant();
}

/*!
    \qmlproperty bool MouseArea::hoverEnabled
    This property holds whether hover events are handled.

    By default, mouse events are only handled in response to a button event, or when a button is
    pressed.  Hover enables handling of all mouse events even when no mouse button is
    pressed.

    This property affects the containsMouse property and the onEntered, onExited and onPositionChanged signals.
*/
bool QxMouseArea::hoverEnabled() const
{
    return acceptHoverEvents();
}

void QxMouseArea::setHoverEnabled(bool h)
{
    Q_D(QxMouseArea);
    if (h == acceptHoverEvents())
        return;

    setAcceptHoverEvents(h);
    emit hoverEnabledChanged();
    if (d->hovered != isUnderMouse())
        setHovered(!d->hovered);
}

/*!
    \qmlproperty bool MouseArea::containsMouse
    This property holds whether the mouse is currently inside the mouse area.

    \warning This property is not updated if the area moves under the mouse: \e containsMouse will not change.
    In addition, if hoverEnabled is false, containsMouse will only be valid when the mouse is pressed.
*/
bool QxMouseArea::hovered() const
{
    Q_D(const QxMouseArea);
    return d->hovered;
}

/*!
    \qmlproperty bool MouseArea::pressed
    This property holds whether the mouse area is currently pressed.
*/
bool QxMouseArea::pressed() const
{
    Q_D(const QxMouseArea);
    return d->pressed;
}

void QxMouseArea::setHovered(bool h)
{
    Q_D(QxMouseArea);
    if (d->hovered != h) {
        d->hovered = h;
        emit hoveredChanged();
        d->hovered ? emit entered() : emit exited();
    }
}

/*!
    \qmlproperty Qt::MouseButtons MouseArea::acceptedButtons
    This property holds the mouse buttons that the mouse area reacts to.

    The available buttons are:
    \list
    \o Qt.LeftButton
    \o Qt.RightButton
    \o Qt.MiddleButton
    \endlist

    To accept more than one button the flags can be combined with the
    "|" (or) operator:

    \code
    MouseArea { acceptedButtons: Qt.LeftButton | Qt.RightButton }
    \endcode

    The default is to accept the Left button.
*/
Qt::MouseButtons QxMouseArea::acceptedButtons() const
{
    return acceptedMouseButtons();
}

void QxMouseArea::setAcceptedButtons(Qt::MouseButtons buttons)
{
    if (buttons != acceptedMouseButtons()) {
        setAcceptedMouseButtons(buttons);
        emit acceptedButtonsChanged();
    }
}

bool QxMouseArea::setPressed(bool p)
{
    Q_D(QxMouseArea);
    bool dragged = d->drag && d->drag->active();
    bool isclick = d->pressed == true && p == false && dragged == false && d->hovered == true;

    if (d->pressed != p) {
        d->pressed = p;
        QxMouseEvent me(d->lastPos.x(), d->lastPos.y(), d->lastButton, d->lastButtons, d->lastModifiers, isclick, d->longPress);
        if (d->pressed) {
            emit pressed(&me);
            me.setX(d->lastPos.x());
            me.setY(d->lastPos.y());
            emit mousePositionChanged(&me);
        } else {
            emit released(&me);
            me.setX(d->lastPos.x());
            me.setY(d->lastPos.y());
            if (isclick && !d->longPress)
                emit clicked(&me);
        }

        emit pressedChanged();
        return me.isAccepted();
    }
    return false;
}

QxDrag *QxMouseArea::drag()
{
    Q_D(QxMouseArea);
    if (!d->drag)
        d->drag = new QxDrag;
    return d->drag;
}

/*!
    \qmlproperty Item MouseArea::drag.target
    \qmlproperty bool MouseArea::drag.active
    \qmlproperty enumeration MouseArea::drag.axis
    \qmlproperty real MouseArea::drag.minimumX
    \qmlproperty real MouseArea::drag.maximumX
    \qmlproperty real MouseArea::drag.minimumY
    \qmlproperty real MouseArea::drag.maximumY

    drag provides a convenient way to make an item draggable.

    \list
    \i \c target specifies the item to drag.
    \i \c active specifies if the target item is being currently dragged.
    \i \c axis specifies whether dragging can be done horizontally (Drag.XAxis), vertically (Drag.YAxis), or both (Drag.XandYAxis)
    \i the minimum and maximum properties limit how far the target can be dragged along the corresponding axes.
    \endlist

    The following example uses drag to reduce the opacity of an image as it moves to the right:
    \snippet doc/src/snippets/declarative/drag.qml 0
*/

