/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXITEM_H
#define QXITEM_H

#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/qdeclarativecomponent.h>

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtGui/qgraphicsitem.h>
#include <QtGui/qfont.h>
#include <QtGui/qaction.h>

#include "qmlscene_global.h"

class Node;
class QDeclarativeState;
class QxAnchorLine;
class QDeclarativeTransition;
class QxKeyEvent;
class QxAnchors;
class QxItemPrivate;
class QT_SCENEGRAPH_EXPORT QxItem : public QObject, public QDeclarativeParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QDeclarativeParserStatus)

    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible NOTIFY visibleChanged FINAL)
    Q_PROPERTY(QPointF pos READ pos WRITE setPos FINAL)
    Q_PROPERTY(qreal x READ x WRITE setX NOTIFY xChanged FINAL)
    Q_PROPERTY(qreal y READ y WRITE setY NOTIFY yChanged FINAL)
    Q_PROPERTY(qreal z READ zValue WRITE setZValue NOTIFY zChanged FINAL)
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity NOTIFY opacityChanged FINAL)
    Q_PROPERTY(qreal scale READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged RESET resetWidth FINAL)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged RESET resetHeight FINAL)
    Q_PROPERTY(bool clip READ clip WRITE setClip NOTIFY clipChanged)
    Q_PROPERTY(QDeclarativeListProperty<QxItem> children READ children)
    Q_PROPERTY(TransformOrigin transformOrigin READ transformOrigin WRITE setTransformOrigin NOTIFY transformOriginChanged)
    Q_PROPERTY(QDeclarativeListProperty<QGraphicsTransform> transform READ transformations DESIGNABLE false FINAL)
    Q_PROPERTY(QxItem *parent READ parentItem WRITE setParentItem NOTIFY parentChanged DESIGNABLE false FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QDeclarativeListProperty<QObject> data READ data DESIGNABLE false)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QDeclarativeListProperty<QObject> resources READ resources DESIGNABLE false)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QDeclarativeListProperty<QDeclarativeState> states READ states DESIGNABLE false)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QDeclarativeListProperty<QDeclarativeTransition> transitions READ transitions DESIGNABLE false)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QString state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(QRectF childrenRect READ childrenRect NOTIFY childrenRectChanged DESIGNABLE false FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchors * anchors READ anchors DESIGNABLE false CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine left READ left CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine right READ right CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine horizontalCenter READ horizontalCenter CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine top READ top CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine bottom READ bottom CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine verticalCenter READ verticalCenter CONSTANT FINAL)
    Q_PRIVATE_PROPERTY(QxItem::d_func(), QxAnchorLine baseline READ baseline CONSTANT FINAL)
    Q_PROPERTY(qreal baselineOffset READ baselineOffset WRITE setBaselineOffset NOTIFY baselineOffsetChanged)
    Q_PROPERTY(bool focus READ hasFocus WRITE setFocus NOTIFY focusChanged FINAL)
    Q_PROPERTY(bool wantsFocus READ wantsFocus NOTIFY wantsFocusChanged)
    Q_PROPERTY(bool smooth READ smooth WRITE setSmooth NOTIFY smoothChanged)

    // We might remove this property later.
    Q_PROPERTY(bool childrenDoNotOverlap READ childrenDoNotOverlap WRITE setChildrenDoNotOverlap NOTIFY childrenDoNotOverlapChanged)

    Q_ENUMS(TransformOrigin)
    Q_CLASSINFO("DefaultProperty", "data")

public:
    enum TransformOrigin {
        TopLeft, Top, TopRight,
        Left, Center, Right,
        BottomLeft, Bottom, BottomRight
    };

    QxItem(QxItem *parent = 0);
    virtual ~QxItem();

    QTransform itemTransform(const QxItem *, bool * = 0) const;

    QPointF mapFromScene(const QPointF &) const;
    QPointF mapToScene(const QPointF &) const;
    QRectF mapToScene(const QRectF &) const;
    QPointF mapToItem(QxItem *, const QPointF &) const;
    QPointF mapFromItem(QxItem *, const QPointF &) const;

    bool isEnabled() const;
    void setEnabled(bool e);

    bool isVisible() const;
    void setVisible(bool);

    QRectF geometry() const;
    QPointF pos() const;
    QSizeF size() const;
    void setPos(const QPointF &);
    void setSize(const QSizeF &);

    qreal x() const;
    qreal y() const;
    void setX(qreal);
    void setY(qreal);

    qreal zValue() const;
    void setZValue(qreal);

    qreal width() const;
    qreal implicitWidth() const;
    bool widthValid() const;
    void setWidth(qreal);
    void setImplicitWidth(qreal);
    void resetWidth();
    qreal height() const;
    qreal implicitHeight() const;
    bool heightValid() const;
    void setHeight(qreal);
    void setImplicitHeight(qreal);
    void resetHeight();

    bool clip() const;
    void setClip(bool);

    TransformOrigin transformOrigin() const;
    void setTransformOrigin(TransformOrigin);

    qreal opacity() const;
    qreal renderOpacity() const;
    void setOpacity(qreal);

    qreal scale() const;
    void setScale(qreal);

    qreal rotation() const;
    void setRotation(qreal);

    QTransform transform() const;
    void setTransform(const QTransform &);

    QTransform combinedTransform() const;

    void stackBefore(QxItem *);

    QPointF transformOriginPoint() const;

    QDeclarativeListProperty<QGraphicsTransform> transformations();

    QList<QxItem*> childItems() const;
    QDeclarativeListProperty<QxItem> children();

    Qt::MouseButtons acceptedMouseButtons() const;
    void setAcceptedMouseButtons(Qt::MouseButtons);
    void setFiltersChildEvents(bool);
    void grabMouse();
    void ungrabMouse();
    QxItem *mouseGrabberItem() const;

    bool hasFocus() const;
    void setFocus(Qt::FocusReason);
    void clearFocus();
    bool isActive() const;
    void setActive(bool);
    bool isFocusScope() const;
    QxItem *focusItem() const;

    bool acceptHoverEvents() const;
    void setAcceptHoverEvents(bool);
    bool isUnderMouse() const;

    Node *paintNode() const;
    void setPaintNode(Node *);

    bool childrenDoNotOverlap() const;
    void setChildrenDoNotOverlap(bool noOverlap);

    QxItem *parentItem() const;
    void setParentItem(QxItem *parent);

    QRectF childrenRect();

    qreal baselineOffset() const;
    void setBaselineOffset(qreal);

    bool smooth() const;
    void setSmooth(bool);

    QRectF boundingRect() const;

    bool wantsFocus() const;
    void setFocus(bool);

    bool keepMouseGrab() const;
    void setKeepMouseGrab(bool);

    Q_INVOKABLE QScriptValue mapFromItem(const QScriptValue &item, qreal x, qreal y) const;
    Q_INVOKABLE QScriptValue mapToItem(const QScriptValue &item, qreal x, qreal y) const;
    Q_INVOKABLE void forceFocus();
    Q_INVOKABLE QxItem *childAt(qreal x, qreal y) const;

Q_SIGNALS:
    void xChanged();
    void yChanged();
    void zChanged();
    void widthChanged();
    void heightChanged();

    void visibleChanged();
    void opacityChanged();
    void scaleChanged();
    void rotationChanged();
    void transformOriginChanged();
    void clipChanged();

    void childrenDoNotOverlapChanged();
    void childrenChanged();
    void childrenRectChanged(const QRectF &);
    void baselineOffsetChanged(qreal);
    void stateChanged(const QString &);
    void focusChanged(bool);
    void wantsFocusChanged(bool);
    void parentChanged(QxItem *);
    void smoothChanged(bool);
    void enabledChanged();

protected slots:
    void updateTransform();

protected:
    QxItem(QxItemPrivate &dd, QxItem *parent = 0);

    // XXX akennedy this is horrible - we should do it differently
    virtual QVariant itemChange(QGraphicsItem::GraphicsItemChange, const QVariant &);
    virtual void itemParentHasChanged();
    virtual void itemOpacityHasChanged();
    virtual void itemVisibleHasChanged();

    virtual void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);
    virtual void renderOpacityChanged(qreal newOpacity, qreal oldOpacity);
    virtual void transformChanged(const QTransform &newTransform, const QTransform &oldTransform);
    virtual void smoothChange(bool newSmooth, bool oldSmooth);

    virtual bool event(QEvent *);
    virtual bool sceneEvent(QEvent *);
    virtual bool sceneEventFilter(QxItem *i, QEvent *e);

    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    virtual void wheelEvent(QGraphicsSceneWheelEvent *event);
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent*);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent*);
    virtual void focusInEvent(QFocusEvent *event);
    virtual void focusOutEvent(QFocusEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);

    bool isComponentComplete() const;

    virtual void classBegin();
    virtual void componentComplete();
    virtual void inputMethodEvent(QInputMethodEvent *);
    virtual QVariant inputMethodQuery(Qt::InputMethodQuery query) const;
    void keyPressPreHandler(QKeyEvent *);
    void keyReleasePreHandler(QKeyEvent *);
    void inputMethodPreHandler(QInputMethodEvent *);

private:
    friend class QxGraphicsView;
    friend class QxGraphicsViewPrivate;
    friend class SceneGraphItem;
    friend class SceneGraphItemPrivate;
    Q_DISABLE_COPY(QxItem)
    Q_DECLARE_PRIVATE(QxItem)
};

QML_DECLARE_TYPE(QxItem)

#endif // QXITEM_H
