/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXTEXT_P_H
#define QXTEXT_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qxitem_p.h"
#include "qxtext_p.h"

#include <QtDeclarative/qdeclarative.h>
#include <QtGui/qtextlayout.h>

class TextNode ;
class QTextLayout;
class QTextDocumentWithImageResources;
class QTextFrame;

class QxTextPrivate : public QxItemPrivate
{
    Q_DECLARE_PUBLIC(QxText)
public:
    QxTextPrivate()
        : textStyle(QxText::Normal)
        , alignment(Qt::AlignTop | Qt::AlignLeft)
        , wrapMode(QxText::NoWrap)
        , textFormat(QxText::AutoText)
        , elideMode(QxText::ElideNone)
        , usePixmapCache(false)
        , node(0)
    {
    }

    void setImplicitSizeBasedOnContents();
    void ensureNodeHasTextDocument();

    static inline QxTextPrivate *get(QxText *t) {
        return t->d_func();
    }

    QString text;
    QFont font;
    QColor color;
    QxText::TextStyle textStyle;
    QColor styleColor;
    Qt::Alignment alignment;
    QxText::WrapMode wrapMode;
    QxText::TextFormat textFormat;
    QxText::TextElideMode elideMode;
    bool usePixmapCache;

    TextNode *node;
    QString activeLink;
};

#endif
