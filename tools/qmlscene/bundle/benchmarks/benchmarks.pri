symbian {
    benchmarks.sources = $$DECLARATIVE_UI_EXAMPLES/benchmarks
    benchmarks.path = ./qmlscene-resources

    DEPLOYMENT += benchmarks

} else {
    RESOURCES += $$PWD/benchmarks.qrc
}
