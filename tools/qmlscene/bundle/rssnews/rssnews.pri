symbian {

	rssnews.sources = $$[QT_INSTALL_DEMOS]/declarative/rssnews
	rssnews.path = ./qmlscene-resources

	DEPLOYMENT += rssnews

} else {

	RESOURCES += $$PWD/rssnews.qrc
	
}