symbian {
	simplephone.sources = $$DECLARATIVE_UI_EXAMPLES/simplephone
	simplephone.path = ./qmlscene-resources

	DEPLOYMENT += simplephone
	
} else {
	RESOURCES += $$PWD/simplephone.qrc
}