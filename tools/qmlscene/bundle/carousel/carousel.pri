symbian {

	carousel.sources = $$DECLARATIVE_UI_EXAMPLES/carousel
	carousel.path = ./qmlscene-resources

	DEPLOYMENT += carousel

} else {

	RESOURCES += $$PWD/carousel.qrc
	
}