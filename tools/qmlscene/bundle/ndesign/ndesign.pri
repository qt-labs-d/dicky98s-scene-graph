symbian {
    ndesign.sources = $$DECLARATIVE_UI_EXAMPLES/ndesign
    ndesign.path = ./qmlscene-resources

    DEPLOYMENT += ndesign

} else {
    RESOURCES += $$PWD/ndesign.qrc
}
