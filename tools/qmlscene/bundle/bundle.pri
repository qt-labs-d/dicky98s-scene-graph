DEFINES += QMLSCENE_BUNDLE

RESOURCES += $$PWD/bundle.qrc              

symbian:isEmpty(DECLARATIVE_UI_EXAMPLES) {
	warning("Using default DECLARATIVE_UI_EXAMPLES directory. Set environment variable before running qmake to override")
	DECLARATIVE_UI_EXAMPLES=$$PWD/../../../../declarative-ui-examples
}

include (simplephone/simplephone.pri)
include (carousel/carousel.pri)
include (samegame/samegame.pri)
include (flickr/flickr.pri)
include (snake/snake.pri)
include (rssnews/rssnews.pri)
include (socialphonebook/socialphonebook.pri)
include (corkboards/corkboards.pri)
include (benchmarks/benchmarks.pri)
include (ndesign/ndesign.pri)
