/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the test suite of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include <QDeclarativeEngine>
#include <QDeclarativeComponent>
#include <QDebug>
#include <QApplication>
#include <QTime>
#include <QDeclarativeContext>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>

#include <canvas/qxgraphicsview.h>
#include <qxitem.h>

class Timer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDeclarativeComponent *component READ component WRITE setComponent)

public:
    Timer();

    QDeclarativeComponent *component() const;
    void setComponent(QDeclarativeComponent *);

    static Timer *timerInstance();

    void run(uint);

    bool willParent() const;
    void setWillParent(bool p);

    void setDisplay(bool d);

private:
    void runTest(QDeclarativeContext *, uint);

    QDeclarativeComponent *m_component;
    static Timer *m_timer;

    bool m_willparent;
    bool m_display;
    QGraphicsView m_view;
    QGraphicsScene m_scene;
    QGraphicsRectItem m_item;
    QxGraphicsView m_xview;
};
QML_DECLARE_TYPE(Timer);

Timer *Timer::m_timer = 0;

Timer::Timer()
: m_component(0), m_willparent(false), m_display(false)
{
    if (m_timer)
        qWarning("Timer: Timer already registered");
    m_timer = this;

    m_scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    m_scene.addItem(&m_item);
    m_view.setScene(&m_scene);
}

QDeclarativeComponent *Timer::component() const
{
    return m_component;
}

void Timer::setComponent(QDeclarativeComponent *c)
{
    m_component = c;
}

Timer *Timer::timerInstance()
{
    return m_timer;
}

void Timer::run(uint iterations)
{
    QDeclarativeContext context(qmlContext(this));

    QObject *o = m_component->create(&context);
    QGraphicsObject *go = qobject_cast<QGraphicsObject *>(o);
    if (go) {
        qWarning() << "Using graphicsview...";
        if (m_willparent)
            go->setParentItem(&m_item);
        if (m_display)
           m_view.show();
    }
    QxItem *xgo = qobject_cast<QxItem *>(o);
    if (xgo) {
        qWarning() << "Using scenegraph...";
        if (m_willparent)
            xgo->setParentItem(m_xview.root());
        if (m_display)
            m_xview.show();
    }

    if (!m_display) {
        delete o;
        runTest(&context, iterations);
    } else
        qApp->exec();
}

bool Timer::willParent() const
{
    return m_willparent;
}

void Timer::setWillParent(bool p)
{
    m_willparent = p;
}

void Timer::setDisplay(bool d)
{
    m_display = d;
    m_willparent = d;
}

void Timer::runTest(QDeclarativeContext *context, uint iterations)
{
    QTime t;
    t.start();
    for (uint ii = 0; ii < iterations; ++ii) {
        QObject *o = m_component->create(context);
        QGraphicsObject *go = qobject_cast<QGraphicsObject *>(o);
        if (m_willparent && go)
            go->setParentItem(&m_item);
        QxItem *xgo = qobject_cast<QxItem *>(o);
        if (m_willparent && xgo)
            xgo->setParentItem(m_xview.root());
        delete o;
    }

    int e = t.elapsed();

    qWarning() << "Total:" << e << "ms, Per iteration:" << qreal(e) / qreal(iterations) << "ms";

}

void usage(const char *name)
{
    qWarning("Usage: %s [-iterations <count>] [-parent] <qml file>", name);
    exit(-1);
}

int main(int argc, char ** argv)
{
    QApplication app(argc, argv);

    qmlRegisterType<Timer>("QmlTime", 1, 0, "Timer");

    uint iterations = 1024;
    QString filename;
    bool willParent = false;
    bool originalQml = false;
    bool display = false;

    for (int ii = 1; ii < argc; ++ii) {
        QByteArray arg(argv[ii]);

        if (arg == "-iterations") {
            if (ii + 1 < argc) {
                ++ii;
                QByteArray its(argv[ii]);
                bool ok = false;
                iterations = its.toUInt(&ok);
                if (!ok)
                    usage(argv[0]);
            } else {
                usage(argv[0]);
            }
        } else if (arg == "-parent") {
            willParent = true;
        } else if (arg == "--original-qml") {
            originalQml = true;
        } else if (arg == "-show") {
            display = true;
        } else {
            filename = QLatin1String(argv[ii]);
        }
    }

    if (filename.isEmpty())
        usage(argv[0]);

    QDeclarativeEngine engine;
    if (!originalQml)
        QxGraphicsView(); // Force call to qt_scenegraph_register_types()

    QDeclarativeComponent component(&engine, filename);
    if (component.isError()) {
        qWarning() << component.errors();
        return -1;
    }

    QObject *obj = component.create();
    if (!obj) {
        qWarning() << component.errors();
        return -1;
    }

    Timer *timer = Timer::timerInstance();
    if (!timer) {
        qWarning() << "A Tester.Timer instance is required.";
        return -1;
    }

    timer->setWillParent(willParent);
    timer->setDisplay(display);

    if (!timer->component()) {
        qWarning() << "The timer has no component";
        return -1;
    }

    timer->run(iterations);

    return 0;
}

#include "qmltime.moc"
