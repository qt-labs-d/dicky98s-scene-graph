/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import Qt 4.7
import QmlTime 1.0 as QmlTime

Item {

    property string online_status: "online"
    property string number: "555-3294"
    property string name: "John Smith"
    property string avatar: "avatar_001.png"
    property int index: 3

    Item {
        id: list
        width: 480
    }

    QmlTime.Timer {
        // delegate from research/declarative-ui-examples/contacts-perf
        component: Component {
            id: delegate
            Item {
                id: wrapper
                width: list.width; height: 72
                Rectangle {
                    anchors.fill: parent
                    color: "#00c0df"
                    opacity: (index % 2) ? 0.3 : 0
                }
                Image {
                    x: 7; y: 7; width: 58; height: 58
                    smooth: true
                    source: "content/images/avatars/" + avatar
                }
                Column {
                    x: 80; y: 7
                    Text { text: name; font.pixelSize: 16 }
                    Text { text: number }
                }
                Image {
                    x: wrapper.width - 32; y: 7; width: 21; height: 21
                    smooth: true
                    source: "content/images/blue/blue_contact_status_" + online_status + "_33x33px.svg"
                }
            }
        }
    }
}
