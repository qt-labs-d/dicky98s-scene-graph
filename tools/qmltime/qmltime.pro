load(qttest_p4)
TEMPLATE = app
TARGET = qmltime
QT += opengl declarative script network
macx:CONFIG -= app_bundle
DESTDIR=../../bin

MOC_DIR = tmp.moc
OBJECTS_DIR = tmp.obj

include($$PWD/../../src/scenegraph_include.pri)

symbian {
    contains(QT_EDITION, OpenSource) {
        TARGET.CAPABILITY = LocalServices NetworkServices ReadUserData UserEnvironment WriteUserData
    } else {
        TARGET.CAPABILITY = All -Tcb
    }
}

SOURCES += qmltime.cpp
