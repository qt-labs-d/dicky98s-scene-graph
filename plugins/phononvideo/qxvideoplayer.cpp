/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxvideoplayer_p.h"
#include "default_videonode.h"

QxVideoPlayer::QxVideoPlayer(QxItem *parent)
    : QxItem(parent)
{
#if defined(QT_OPENGL_ES_2)
    m_node = new DefaultVideoNode(DefaultVideoNode::SystemMemoryUpload);
#else
    m_node = new DefaultVideoNode(DefaultVideoNode::PixelBufferObjectUpload);
#endif
    setPaintNode(m_node);
    connect(m_node, SIGNAL(stateChanged()), this, SIGNAL(stateChanged()));
    connect(m_node, SIGNAL(lengthChanged()), this, SIGNAL(lengthChanged()));
    connect(m_node, SIGNAL(currentTimeChanged()), this, SIGNAL(currentTimeChanged()));
    connect(m_node, SIGNAL(nativeSizeChanged()), this, SIGNAL(nativeWidthChanged()));
    connect(m_node, SIGNAL(nativeSizeChanged()), this, SIGNAL(nativeHeightChanged()));
}

QxVideoPlayer::~QxVideoPlayer()
{
    delete m_node;
}

void QxVideoPlayer::setSource(const QUrl &url)
{
    if (url == m_node->source())
        return;
    m_node->setSource(url);
    emit sourceChanged();
}

QUrl QxVideoPlayer::source() const
{
    return m_node->source();
}

void QxVideoPlayer::setPlaying(bool playing)
{
    if (playing == m_node->isPlaying())
        return;
    m_node->setPlaying(playing);
    emit playingChanged();
}

bool QxVideoPlayer::isPlaying() const
{
    return m_node->isPlaying();
}

void QxVideoPlayer::setCurrentTime(int time)
{
    if (time == m_node->currentTime())
        return;
    m_node->setCurrentTime(time);
    emit currentTimeChanged();
}

int QxVideoPlayer::currentTime() const
{
    return int(m_node->currentTime());
}

int QxVideoPlayer::length() const
{
    return int(m_node->length());
}

QxVideoPlayer::State QxVideoPlayer::state() const
{
    return m_node->state();
}

void QxVideoPlayer::setSourceWidth(int width)
{
    QSize size = m_node->sourceSize();
    if (width == size.width())
        return;
    size.setWidth(width);
    m_node->setSourceSize(size);
    emit sourceWidthChanged();
}

void QxVideoPlayer::setSourceHeight(int height)
{
    QSize size = m_node->sourceSize();
    if (height == size.height())
        return;
    size.setHeight(height);
    m_node->setSourceSize(size);
    emit sourceHeightChanged();
}

int QxVideoPlayer::sourceWidth() const
{
    return m_node->sourceSize().width();
}

int QxVideoPlayer::sourceHeight() const
{
    return m_node->sourceSize().height();
}

int QxVideoPlayer::nativeWidth() const
{
    return m_node->nativeSize().width();
}

int QxVideoPlayer::nativeHeight() const
{
    return m_node->nativeSize().height();
}

void QxVideoPlayer::setFillMode(FillMode mode)
{
    if (mode == m_node->fillMode())
        return;
    m_node->setFillMode(mode);
    emit fillModeChanged();
}

QxVideoPlayer::FillMode QxVideoPlayer::fillMode() const
{
    return m_node->fillMode();
}

void QxVideoPlayer::setMute(bool mute)
{
    if (mute == m_node->isMute())
        return;
    m_node->setMute(mute);
    emit muteChanged();
}

bool QxVideoPlayer::isMute() const
{
    return m_node->isMute();
}

void QxVideoPlayer::setVolume(qreal volume)
{
    if (volume == m_node->volume())
        return;
    m_node->setVolume(volume);
    emit volumeChanged();
}

qreal QxVideoPlayer::volume() const
{
    return m_node->volume();
}

void QxVideoPlayer::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QxItem::geometryChanged(newGeometry, oldGeometry);
    if (newGeometry.size() != oldGeometry.size())
        m_node->setRect(QRectF(0, 0, newGeometry.width(), newGeometry.height()));
}

void QxVideoPlayer::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    QxItem::renderOpacityChanged(newOpacity, oldOpacity);
    m_node->setOpacity(newOpacity);
}

void QxVideoPlayer::smoothChange(bool newSmooth, bool oldSmooth)
{
    QxItem::smoothChange(newSmooth, oldSmooth);
    m_node->setLinearFiltering(newSmooth);
}
