import QtQuick 2.0


Item
{
    width: 800
    height: 600

    Rectangle {
        id: box
        color: mouseArea.pressed ? "black" : "steelblue"
        radius: 10
        width: 100
        height: 100
        border.color: "red"
        border.width: 3

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            drag.target: box
        }
    }

}
