/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Item {
    width: 400
    height: 400

    property int t: 0
    property bool v: false
    NumberAnimation on t {
        loops: Animation.Infinite
        from: 0
        to: 2 * 3 * 5 * 7 * 11 * 13
        duration: 40000
    }

    SequentialAnimation on v {
        loops: Animation.Infinite
        PropertyAction { value: false }
        PauseAnimation { duration: 100 }
        PropertyAction { value: true }
        PauseAnimation { duration: 1000 }
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: 20
        color: "yellow"

        Rectangle {
            anchors.fill: parent
            anchors.margins: 30
            color: "blue"
            z: 1
            visible: ((t / 2) & 1) == 0 || v
        }

        Rectangle {
            anchors.fill: parent
            anchors.margins: 10
            color: "green"
            visible: ((t / 3) & 1) == 0 || v
        }

        Rectangle {
            anchors.fill: parent
            anchors.margins: -20
            color: "red"
            z: -1
            visible: ((t / 5) & 1) == 0 || v
        }

        Rectangle {
            anchors.fill: parent
            anchors.margins: 20
            color: "cyan"
            visible: ((t / 7) & 1) == 0 || v
        }

        Rectangle {
            anchors.fill: parent
            anchors.margins: 40
            color: "magenta"
            z: 1
            visible: ((t / 11) & 1) == 0 || v
        }

        Rectangle {
            anchors.fill: parent
            anchors.margins: -10
            color: "orange"
            z: -1
            visible: ((t / 13) & 1) == 0 || v
        }
    }
}
