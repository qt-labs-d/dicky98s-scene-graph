import QtQuick 1.0

Item {
    width: 600
    height: 400

    Rectangle {
        width: 100
        height: 100
        gradient: Gradient {
            GradientStop { position: 0; color: "red" }
            GradientStop { position: 1; color: "blue" }
        }
    }
}
