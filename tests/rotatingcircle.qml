import QtQuick 2.0

Item {
    width: 800
    height: 600

    Rectangle {
        width: 100
        height: 100
        radius: 50

        color: "black"
        border.color: "red"
        border.width: 4

        property real t;

        SequentialAnimation on t {
            NumberAnimation {
                from: 0
                to: Math.PI * 2 - Math.PI * 2 / 2500
                duration: 2500
            }
            PauseAnimation { duration: 2000 }

            loops: Animation.Infinite
        }


        x: parent.width / 2 + parent.width / 3 * Math.cos(t) - width / 2;
        y: parent.height / 2 + parent.height / 3 * Math.sin(t) - height / 2;
    }
}
