/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Rectangle {
    width: 360
    height: 540

    gradient: Gradient {
        GradientStop { position: 0; color: "steelblue" }
        GradientStop { position: 1; color: "black" }
    }

    Item {
        id: theItem
        anchors.fill: parent

        Rectangle {
            x: 0;
            y: 0;
            width: 100;
            height: parent.height;
            color: "white";
            opacity: 0.5

            NumberAnimation on width { from: 0; to: theItem.width; loops: Animation.Infinite; duration: 1000 }
        }

        Rectangle {
            x: 100
            y: 100
            width: 100
            height: 100
            color: "white"
            radius: 15
            Text {
                anchors.centerIn: parent
                text: "Click me!"            
            }
            MouseArea {
                anchors.fill: parent
                property bool wasDragged: false
                onPositionChanged: {
                    parent.x += mouse.x - width / 2
                    parent.y += mouse.y - height / 2
                    wasDragged = true
                }
                onPressed: {
                    wasDragged = false
                }
                onClicked: {
                    if (!wasDragged)
                        effectsAreActive ^= true
                }
            }
        }
    }

    property bool effectsAreActive: false
    property real angle: 0

    NumberAnimation on angle {
        from: 0
        to: 360
        duration: 10000
        loops: Animation.Infinite
    }

    ColorizeEffectItem {
        id: topLeftEffect
        anchors.left: parent.left
        anchors.top: parent.top
        width: parent.width / 2
        height: parent.height / 2
        source: maskEffect
        active: effectsAreActive
        blending: true
        color: "red"
        transformOrigin: Item.BottomRight
        rotation: angle
        smooth: true
        MaskEffectItem {
            id: maskEffect
            anchors.fill: parent
            source: theItem
            mask: theMaskItem
            active: effectsAreActive
        }
        Rectangle {
            id: theMaskItem
            anchors.fill: parent
            anchors.margins: 10
            color: "white"
            radius: 50
            border.width: 20
            border.color: "gray"
            visible: effectsAreActive
        }
    }
    ColorizeEffectItem {
        id: topRightEffect
        anchors.left: topLeftEffect.right
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: topLeftEffect.bottom
        source: theItem
        active: effectsAreActive
        blending: true
        color: "green"
        transformOrigin: Item.BottomLeft
        rotation: angle
        smooth: true
    }
    ColorizeEffectItem {
        id: bottomLeftEffect
        anchors.left: parent.left
        anchors.top: topLeftEffect.bottom
        anchors.right: topLeftEffect.right
        anchors.bottom: parent.bottom
        source: theItem
        active: effectsAreActive
        blending: true
        color: "blue"
        transformOrigin: Item.TopRight
        rotation: angle
        smooth: true
    }
    DirectionalBlurEffectItem {
        id: bottomRightEffect
        anchors.left: bottomLeftEffect.right
        anchors.top: topRightEffect.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        source: child
        DirectionalBlurEffectItem {
            id: child
            anchors.fill: parent
            anchors.margins: -10
            source: grandChild
            ColorizeEffectItem {
                id: grandChild
                anchors.fill: parent
                source: theItem
                active: effectsAreActive
                blending: true
                color: "yellow"
                smooth: true
            }
            active: effectsAreActive
            smooth: true
            xStep: 1 / width
            yStep: 0
        }
        active: effectsAreActive
        transformOrigin: Item.TopLeft
        rotation: angle
        smooth: true
        xStep: 0
        yStep: 1 / (height + 2 * margins.height)
        margins: "10x10"
    }


}
