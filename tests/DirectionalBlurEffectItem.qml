/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

ShaderEffectItem
{
    fragmentShader: "varying highp vec2 qt_TexCoord;                                    \n" +
                    "varying highp vec2 qt_TexCoord2;                                   \n" +
                    "varying highp vec2 qt_TexCoord3;                                   \n" +
                    "varying highp vec2 qt_TexCoord4;                                   \n" +
                    "varying highp vec2 qt_TexCoord5;                                   \n" +
                    "varying highp vec2 qt_TexCoord6;                                   \n" +
                    "varying highp vec2 qt_TexCoord7;                                   \n" +
                    "varying highp vec2 qt_TexCoord8;                                   \n" +
                    "uniform sampler2D source;                                          \n" +
                    "void main() {                                                      \n" +
                    "    gl_FragColor = (4. / 20. * texture2D(source, qt_TexCoord)      \n" +
                    "                 + 3. / 20. * texture2D(source, qt_TexCoord2)      \n" +
                    "                 + 2. / 20. * texture2D(source, qt_TexCoord3)      \n" +
                    "                 + 1. / 20. * texture2D(source, qt_TexCoord4)      \n" +
                    "                 + 4. / 20. * texture2D(source, qt_TexCoord5)      \n" +
                    "                 + 3. / 20. * texture2D(source, qt_TexCoord6)      \n" +
                    "                 + 2. / 20. * texture2D(source, qt_TexCoord7)      \n" +
                    "                 + 1. / 20. * texture2D(source, qt_TexCoord8));    \n" +
                    "}"

    vertexShader: "attribute highp vec4 qt_Vertex;                          \n" +
                  "attribute highp vec2 qt_MultiTexCoord0;                  \n" +
                  "uniform highp mat4 qt_Matrix;                            \n" +
                  "uniform highp float xStep;                               \n" +
                  "uniform highp float yStep;                               \n" +
                  "uniform highp float spread;                              \n" +
                  "varying highp vec2 qt_TexCoord;                          \n" +
                  "varying highp vec2 qt_TexCoord2;                         \n" +
                  "varying highp vec2 qt_TexCoord3;                         \n" +
                  "varying highp vec2 qt_TexCoord4;                         \n" +
                  "varying highp vec2 qt_TexCoord5;                         \n" +
                  "varying highp vec2 qt_TexCoord6;                         \n" +
                  "varying highp vec2 qt_TexCoord7;                         \n" +
                  "varying highp vec2 qt_TexCoord8;                         \n" +
                  "void main() {                                            \n" +
                  "    highp vec2 shift = spread * vec2(xStep, yStep);      \n" +
                  "    qt_TexCoord  = 0.66 * shift + qt_MultiTexCoord0;     \n" +
                  "    qt_TexCoord2 = 2.5 * shift + qt_MultiTexCoord0;      \n" +
                  "    qt_TexCoord3 = 4.5 * shift + qt_MultiTexCoord0;      \n" +
                  "    qt_TexCoord4 = 6.5 * shift + qt_MultiTexCoord0;      \n" +
                  "    qt_TexCoord5 = -0.66 * shift + qt_MultiTexCoord0;    \n" +
                  "    qt_TexCoord6 = -2.5 * shift + qt_MultiTexCoord0;     \n" +
                  "    qt_TexCoord7 = -4.5 * shift + qt_MultiTexCoord0;     \n" +
                  "    qt_TexCoord8 = -6.5 * shift + qt_MultiTexCoord0;     \n" +
                  "    gl_Position = qt_Matrix * qt_Vertex;                 \n" +
                  "}"

    property real spread: 1;
    property real xStep: 0 / width;
    property real yStep: 1 / height;
    property variant source: null
}
