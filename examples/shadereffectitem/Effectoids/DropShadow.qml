/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

ShaderEffectItem
{
    fragmentShader:
            "varying highp vec2 qt_TexCoord0;                                       \n" +
            "varying highp vec2 my_TexCoord1;                                       \n" +
            "varying highp vec2 my_TexCoord2;                                       \n" +
            "varying highp vec2 my_TexCoord3;                                       \n" +
            "varying highp vec2 my_TexCoord4;                                       \n" +
            "uniform sampler2D source;                                              \n" +
            "void main() {                                                          \n" +
            "    lowp vec4 pix = texture2D(source, qt_TexCoord0);                   \n" +
            "    lowp float shadow = (texture2D(source, my_TexCoord1).w             \n" +
            "                        + texture2D(source, my_TexCoord2).w            \n" +
            "                        + texture2D(source, my_TexCoord3).w            \n" +
            "                        + texture2D(source, my_TexCoord4).w) * 0.1;    \n" +
            "    gl_FragColor = mix(vec4(0, 0, 0, shadow), pix, pix.w);             \n" +
            "}"

    vertexShader:
            "attribute highp vec4 qt_Vertex;                                        \n" +
            "attribute highp vec2 qt_MultiTexCoord0;                                \n" +
            "uniform highp mat4 qt_ModelViewProjectionMatrix;                       \n" +
            "uniform highp float xOffset;                                           \n" +
            "uniform highp float xDisplacement;                                     \n" +
            "uniform highp float yOffset;                                           \n" +
            "uniform highp float yDisplacement;                                     \n" +
            "varying highp vec2 qt_TexCoord0;                                       \n" +
            "varying highp vec2 my_TexCoord1;                                       \n" +
            "varying highp vec2 my_TexCoord2;                                       \n" +
            "varying highp vec2 my_TexCoord3;                                       \n" +
            "varying highp vec2 my_TexCoord4;                                       \n" +
            "void main() {                                                          \n" +
            "    highp vec2 s1 = vec2(xOffset, yOffset);                            \n" +
            "    highp vec2 s2 = vec2(-xOffset, yOffset);                           \n" +
            "    qt_TexCoord0 = qt_MultiTexCoord0;                                  \n" +
            "    vec2 shadowPos = qt_MultiTexCoord0 + vec2(-xDisplacement, yDisplacement); \n" +
            "    my_TexCoord1 = shadowPos + s1;                                     \n" +
            "    my_TexCoord2 = shadowPos - s1;                                     \n" +
            "    my_TexCoord3 = shadowPos + s2;                                     \n" +
            "    my_TexCoord4 = shadowPos - s2;                                     \n" +
            "    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;            \n" +
            "}"

    property real xOffset: 0.66 / width;
    property real yOffset: 0.66 / height;
    property real xDisplacement: 10 / width;
    property real yDisplacement: 12 / height;
    property variant source
    smooth: true
}
