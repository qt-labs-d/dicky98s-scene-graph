/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

ShaderEffectItem
{
    fragmentShader:
            "varying highp vec2 qt_TexCoord0;                                   \n" +
            "varying highp vec2 my_TexCoord2;                                   \n" +
            "varying highp vec2 my_TexCoord3;                                   \n" +
            "varying highp vec2 my_TexCoord4;                                   \n" +
            "uniform lowp sampler2D source;                                     \n" +
            "uniform highp float qt_Opacity;                                    \n" +
            "void main() {                                                      \n" +
            "    gl_FragColor = qt_Opacity * (texture2D(source, qt_TexCoord0)   \n" +
            "                 + texture2D(source, my_TexCoord2)                 \n" +
            "                 + texture2D(source, my_TexCoord3)                 \n" +
            "                 + texture2D(source, my_TexCoord4)) * 0.25;        \n" +
            "}"

    vertexShader:
            "attribute highp vec4 qt_Vertex;                                    \n" +
            "attribute highp vec2 qt_MultiTexCoord0;                            \n" +
            "uniform highp mat4 qt_ModelViewProjectionMatrix;                   \n" +
            "uniform highp float xOffset;                                       \n" +
            "uniform highp float yOffset;                                       \n" +
            "uniform highp vec2 _normMargins;                                   \n" +
            "varying highp vec2 qt_TexCoord0;                                   \n" +
            "varying highp vec2 my_TexCoord2;                                   \n" +
            "varying highp vec2 my_TexCoord3;                                   \n" +
            "varying highp vec2 my_TexCoord4;                                   \n" +
            "void main() {                                                      \n" +
            "    highp vec2 s1 = vec2(xOffset, yOffset);                        \n" +
            "    highp vec2 s2 = vec2(-xOffset, yOffset);                       \n" +
            "    highp vec2 tcoord = mix(_normMargins,                          \n" +
            "        qt_MultiTexCoord0 - _normMargins, qt_MultiTexCoord0);      \n" +
            "    qt_TexCoord0 = tcoord + s1;                                    \n" +
            "    my_TexCoord2 = tcoord - s1;                                    \n" +
            "    my_TexCoord3 = tcoord + s2;                                    \n" +
            "    my_TexCoord4 = tcoord - s2;                                    \n" +
            "    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;        \n" +
            "}"

    property real xOffset: 0.5 / width
    property real yOffset: 0.5 / height
    property variant _normMargins: Qt.size(margins.width / (2 * margins.width + width), margins.height / (2 * margins.height + height))
    property variant margins: "margins" in source ? source.margins : Qt.size(0, 0)
    active: true
    blending: true
    property variant source
}
