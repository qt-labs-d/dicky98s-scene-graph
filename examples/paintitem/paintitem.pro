TEMPLATE = app
TARGET = paintitem

include(../../src/scenegraph_include.pri)

macx: CONFIG -= app_bundle

SOURCES += main.cpp

CONFIG += console

symbian {
    TARGET.EPOCHEAPSIZE = 0x20000 0x5000000
}
